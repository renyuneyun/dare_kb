## DKB

The DARE Knowledge Base, DKB, prototypes a central part of a framework to accelerate research progress in many applications exploiting modern data, computation and collaboration advances. 

## Motivation 

 

## Tech/framework used

DKB works as a client-server system in python.
The server-side is a Flask application.

## Installation

In order to install the DKB you need to run on python3.8
You can install DKB using pipenv by running:
```
pipenv install -e .
```

If you don't want to use pipenv, you can install by running:
```
python setup.py develop
```
At this point you might encounter the error: *The 'flask' distribution was not found and is required by dare-kb*

Just rerun the previous command to complete the install

## Tests

## How to use?

First of all, you need to run the server part by running:
```
dkb_server
```

## Credits

Credit goes to Malcolm for designing the DKB and its features, Rui Zhao for putting together the client-server system and implementing some of the features,  and Amélie Levray for the implementation of the features.

## License


