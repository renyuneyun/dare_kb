
import DKB_functions as DKBa

#global onto
def main():
    instanceName = "XP_2"

    dkb = DKBa.initDKB(instanceName, reset=True)
    am = dkb.newContext("Amelie Levray", "am")
    dkb.setContext("am")
    
    CA = dkb.newConcept('CA')         #default to top level specialisations of Concept
    CB = dkb.newConcept('CB')
    CC = dkb.newConcept('CC')

    CAA = dkb.newConcept('CAA',
        specialisationOf = 'CA')         # specialisations of CA
    CAB = dkb.newConcept('CAB',
        specialisationOf = 'CA')
    CAC = dkb.newConcept('CAC',
        specialisationOf = 'CA')

    CAAA = dkb.newConcept('CAAA', # and again for testing inclusion below
        specialisationOf = 'CAA')

    CAAAf = dkb.newConcept('CAAAf', # with one required attribute
        specialisationOf = 'CAA',
        required = {'f':'CA'})    # one property f can be CA or any of its specialisations
    CAAAgh = dkb.newConcept('CAAAgh', # with two required attributes
        specialisationOf = 'CAA',
        required = {'g':'CA',    # one property g can be CA or any of its specialisations
          'h': 'CAC'})        # one property h can be CAC
    CAAAij = dkb.newConcept('CAAAij', # test recursive structures
        specialisationOf = 'CAA',
        required = {'i':'CAAAij',        # one property i references instances of this Concept
         'j': 'CAAAij'})

    CAAAijk = dkb.newConcept('CAAAijk', # add recommendations
        specialisationOf = 'CAA',
        required = {'i':'CAAAij',    # one property i references instances of this Concept
        'j': 'CAAAij'},            # and another
        recommended = {'k':'CA'})

    CAAl = dkb.newConcept(
      'CAAl',
      specialisationOf = 'CAA',
      description = 'testing description as an optional parameter'
    )

    MarineOrganism = dkb.newConcept(    # a Concept specialising Concept
      'MarineOrganism',                 # to give us something to use and specialise
      description = '''Anything of any kingdom that has lived or does live in any sea on
      Earth. The plan is to incrementally refine this broad catch all, and to
      organise observational data under more precise taxonomies.'''
    )
    Fish = dkb.newConcept(                # one partition of MarineOrganism
      'Fish',
      specialisationOf = 'MarineOrganism',
      description = '''Fish are gill-bearing aquatic craniate animals that lack limbs with
                              digits.'''
    )
    Teleost = dkb.newConcept(            # in this specialisation demand more info
      'Teleost',                        # the bony fish with scales
      description = '''The teleosts or Teleostei (Greek: teleios, "complete" osteon, "bone")
                    are by far the largest infraclass in the class Actinopterygii, the
                    ray-finned fishes, and make up 96% of all extant species of fish.''',
      specialisationOf = 'Fish',        # instances will represent Teleost species
      required = {
        'LinnaeanSpeciesName': 'String'}     # binomial species name instance of String
    )
    Chondrichthyes = dkb.newConcept(        # and ask politely how big for
      'Chondrichthyes',                        # the cartilaginous fish
      description = '''Chondrichthyes (from Greek chondr- 'cartilage', ichthys 'fish') is a
                    class that contains the cartilaginous fishes. They are jawed
                    vertebrates with paired fins, paired nares, scales, a heart with its
                    chambers in series, and skeletons made of cartilage rather than
                    bone.''',
      specialisationOf = 'Fish',            # instances will represent species
      required = {
        'LinnaeanSpeciesName': 'String'},     #insist on a binomial species name
      recommended = {
        'size': 'Number',        # if available, average adult length in metres
        'mass': 'Number'}        # independently, if available, average adult mass in kilos
                           # later, a time series of these will show species viability
    )
    
    dkb.getConcept("Chondrichthyes")
    
    

    dkb.close()

if __name__ == "__main__":
    main()


## test

#        print("Taille des concepts "+str(len(self.listOfConcepts)))
#        for c in self.listOfConcepts.values():
#            cl = c.my_concept
##            print("Taille des proprietes "+str(len(c.dataProperties)))
##            print(type(cl))
#            print(self._namespace.search(is_a = cl))
#            if (len(c.dataProperties) != 0):
#                print("Properties")
#                for p in c.dataProperties.values():
#                    pr = p.my_property
#    #                print(type(pr))
#                    print(self._namespace.search(is_a = pr))
#                print()
#        t = self.listOfConcepts[0].my_concept
#        print(self._namespace.search(is_a = DataProperty))
#        print(self._namespace.search(subclass_of = t))

#        print("Object Properties")
#        print(list(self._ontology.object_properties()))
#
#        print("Data Properties")
#        print(list(self._ontology.data_properties()))
#
#        print("Annotations Properties")
#        print(self._namespace.search(context_title = "*"))


