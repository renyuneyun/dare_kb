Docker container of DKB
==========

Thie directory contains the files for building the Docker container of DKB.

Usage
------

```
docker build -t dkb .  # Build the image
docker run -d --name dare_kb -p 5000:5000 dkb  # Start the container
```

Options
------

### Build arguments

When building using the `Dockerfile`, there are a few optional arguments to specify:

1. PYPI_MIRROR : The mirror of the PyPI repository
2. DKB_REPO : The (git) repository of the DKB implementation
3. DKB_BRANCH : The branch of the DKB repo to use

An example (use the PyPI mirror provided by Tsinghua University):

```
docker build -t dkb --build-arg PYPI_MIRROR=https://pypi.tuna.tsinghua.edu.cn/simple .
```

### Volume / Persist data

The data is stored under `/opt/dare_kb/data/`. You may want to mount a volume to persist the data. For example:

```
docker run -d --name dare_kb -p 5000:5000 -v /directory/on/your/host/machine:/opt/dare_kb/data dkb  # Start the container
```
