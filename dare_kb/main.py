# import DKB_functions as DKBm  # Use csbridge to replace direct calls
import DKBlib as DKB
import re

def my_regex_for_title2(id):
    result = re.search("([a-zA-Z0-9 \.,])*", id)
    print(result)
    if (result.group() == id):
        return True
    return False

_verbose = True          #iff true also report on standard out
def vprint(toPrint): #print only if _verbose is true
    global _verbose
    if _verbose:
        print(toPrint)


#global onto
def main():

    dkb = DKB.login('ex3KB', 'amelie')
#    print(dkb)
#
    con1 = dkb.new_context('amelie1')
#    print(dkb.status())
###
    con2 = dkb.new_context('amelie2', '2nd context for amelie....')
#    print(dkb.status())
#
    con3 = dkb.new_context('amelie3', '3rd context for amelie', ['kb', 'amelie1'])
    print(dkb.status())
#
#    con4 = dkb.new_context('mal', search_path = ['kb', 'amelie1', 'amelie2', 'amelie3'], owner = 'malcolm')
#    print(dkb.status())

#    con2 = dkb.new_context( 'amelie2', '2nd context for amelie', ['kb', 'amelie1'])
#
#    dkb.enter('amelie2', 'W')
#    amelie2_search_path = dkb.get_search_path()

#    print(amelie2_search_path)
##
#    dkb.enter('amelie1','W')
#    CA = dkb.newConcept('CA')         #default to top level specialisations of Concept
#    CB = dkb.newConcept('CB')       # all 4 dictionaries default to empty
#    CC = dkb.newConcept('CC')

#    i1 = dkb.newInst('CC', 'test1')
#    print(dkb.context_status())
##
#    dkb.enter('mal', 'R')

    dkb.enter('amelie1', 'W')

#    print(dkb.context_status('amelie2'))

    print(dkb.context_status('amelie1'))
    
#    dkb.leave()

#    print(dkb.context_status('amelie1'))

#    dkb.context_reset()
#    dkb.enter('amelie1', 'W')

#    dkb.context_freeze()
#
##    dkb.enter('amelie2','R')
##
##
#    print(dkb.context_status('amelie3'))
##
##    dkb.enter('amelie2', 'W')
##    amelie1_search_path = dkb.get_search_path()
#    print(dkb.context_status('amelie3'))
#
#    print(amelie2_search_path)
#
#    dkb.set_search_path(['kb'])
#
#    amelie2_search_path = dkb.get_search_path()
#
#    print(amelie2_search_path)

    dkb.close()

if __name__ == "__main__":
    main()
