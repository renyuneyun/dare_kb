property_types = {
        'required',
        'recommended',
        'optional',
        }

context_state_list = ['new', 'active', 'frozen', 'deprecated']
instance_state_list = ['new', 'active', 'frozen', 'deprecated', 'discarded']
