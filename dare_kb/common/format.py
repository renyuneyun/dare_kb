'''
Formattors or helper functions to format/deformat information
'''

from datetime import datetime

TIMESTAMPT_FORMAT = '%Y-%m-%d %H:%M:%S.%f'

def timestamp_serialise(timestamp):
    return timestamp.strftime(TIMESTAMPT_FORMAT)

def timestamp_deserialise(timestamp_str):
    return datetime.strptime(timestamp_str, TIMESTAMPT_FORMAT)
