#!/usr/bin/env python3
# -*- coding:utf-8 -*-


'''
This file specifies the bridge between the Client and the Server.
The bridge is intended to mimic the server API call from the client, to decouple the code, for future separation into API calls.
'''

import requests
import json

from datetime import datetime

from dare_kb.common.client_exception import IdentifierWrongError
from dare_kb.common.cs_exception import handle_server_exception, DKBException
from dare_kb.common import format

from typing import (
        Dict,
        List,
        Optional,
        Union,
        )
from dare_kb.common.dkb_typing import (
        PID,
        ConceptComm,
        )


def _expect_server_exception(func):
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except DKBException as e:
            e.coerce()
    return wrapper

server = 'http://localhost:5000'
_reqm = {
        'GET': requests.get,
        'POST': requests.post,
        'PUT': requests.put,
        'DELETE': requests.delete,
        }
def _do_request(method: str, path: str, parse_json=False, **data: dict) -> str:
    if method == 'GET':
        r = _reqm[method](server + path, params=data)
    else:
        headers = {'Content-Type': 'application/json'}
        r = _reqm[method](server + path, headers=headers, data=json.dumps(data))
    msg = r.text
    if parse_json:
        if 'Content-Type' in r.headers and r.headers['Content-Type'] == 'application/json':
            msg = json.loads(r.text)
    if not r.ok:
        msg = r.text
        if 'Content-Type' in r.headers and r.headers['Content-Type'] == 'application/json':
            msg = json.loads(r.text)
        handle_server_exception(r.status_code, msg)
    return msg


@_expect_server_exception
def new_dkb(site_name: str, reset: bool = False, profile = None) -> None:
    _do_request('POST', '/admin/install', site_name=site_name, reset=reset)

@_expect_server_exception
def login(site_name, username, session_id = None) -> str:
    res = _do_request('POST', '/user/login', site_name=site_name, username=username, session_id=session_id)
    return res

@_expect_server_exception
def close(session_id: str) -> None:
    _do_request('POST', '/user/logout', session=session_id)

@_expect_server_exception
def enter(session_id, a_prefix, rw) -> None:
    _do_request('POST', '/user/current_context', session=session_id, prefix=a_prefix, rw=rw)

@_expect_server_exception
def leave(session_id) -> None:
    _do_request('DELETE', '/user/current_context', session=session_id)

@_expect_server_exception
def get_search_path(session_id) -> List[str]:
    res = _do_request('GET', '/user/search_path', parse_json=True, session=session_id).strip()
    return res.split('\n') if res else []

@_expect_server_exception
def set_search_path(session_id, new_search_path: Union[str,List[str]]) -> None:
    if not isinstance(new_search_path, list):
        new_search_path = [new_search_path]
    _do_request('POST', '/user/search_path', session=session_id, search_path=new_search_path)

@_expect_server_exception
def new_context(session_id, prefix, title=None, search_path=None, owner = None) -> str:
    if not prefix: # Server would return 404, so we hijack thi in advance
        raise IdentifierWrongError('A context must have a non-empty prefix')
    res = _do_request('POST', '/context/'+prefix, parse_json=True, session=session_id, title=title, search_path=search_path, owner=owner)
    return res

@_expect_server_exception
def context_reset(session_id, prefix = None) -> None:
    _do_request('DELETE', '/context/reset/', session=session_id, prefix=prefix)

@_expect_server_exception
def context_freeze(session_id, prefix = None) -> None:
    _do_request('DELETE', '/context/freeze/', session=session_id, prefix=prefix)

@_expect_server_exception
def status(session_id) -> Dict:
    res = _do_request('GET', '/user/status', session=session_id)
    return json.loads(res)

@_expect_server_exception
def context_status(session_id, prefix = None) -> Dict:
    res = _do_request('GET', '/context/status/', session=session_id, prefix=prefix)
    return json.loads(res)

@_expect_server_exception
def newConcept(session_id, preciseTerm, specialisationOf = None, extra = dict(), mutability = "mutable", description = None, required = dict(), recommended = dict(), optional = dict(), translation = dict()) -> PID:
    res = _do_request('POST', '/concept/'+preciseTerm, parse_json=True, session=session_id, specialisationOf=specialisationOf, extra=extra, mutability=mutability, description=description, required=required, recommended=recommended, optional=optional, translation=translation)
    return res

@_expect_server_exception
def getConcept(session_id, identity, base=None) -> ConceptComm:
    res = _do_request('GET', '/concept/'+identity, session=session_id, base=base)
    return json.loads(res)

@_expect_server_exception
def newInst(session_id, cls, name, **extra) -> PID:
    extra['class'] = cls  # Writing this because we can't write `class=cls` (syntax error)
    res = _do_request('POST', '/instance/'+name, parse_json=True, session=session_id, **extra)
    return res

@_expect_server_exception
def getInst(session_id, identity):
    res = _do_request('GET', '/instance/'+identity, session=session_id)
    return json.loads(res)

@_expect_server_exception
def get(session_id, identity):
    res = _do_request('GET', '/entry/'+identity, session=session_id)
    return json.loads(res)

@_expect_server_exception
def find(session_id, query, pid_only=False, only_these=None):
    res = _do_request('POST', '/entry', session=session_id, query=query, pid_only=pid_only, only_these=only_these)
    return json.loads(res)


def login_obj(site_name, username, session_id=None, raw_data=True) -> 'DKBAPIUserHandle':
    return DKBAPIUserHandle(login(site_name, username, session_id), raw_data=raw_data)


# TODO: For all the `XXXHandle`: change to separate classes and function calls

class DKBAPIHandle:

    def __init__(self, session_id: str, raw_data=True):
        self.session_id = session_id
        self.raw_data = raw_data

    def close(self):
        close(self.session_id)


# TODO
# class DKBAdminHandle(DKBHandle):

#     def deprecate(self) -> None:
#         self._server_kb.deprecate()


class DKBAPIUserHandle(DKBAPIHandle):

    def get_search_path(self) -> List[str]:
        return get_search_path(self.session_id)

    def set_search_path(self, new_search_path: Union[str,List[str]]) -> None:
        set_search_path(self.session_id, new_search_path)

    def enter(self, a_prefix, rw) -> None:
        enter(self.session_id, a_prefix, rw)

    def leave(self) -> None:
        leave(self.session_id)

    def context_reset(self, a_prefix = None) -> None:
        context_reset(self.session_id, a_prefix)

    def context_freeze(self, a_prefix = None) -> None:
        context_freeze(self.session_id, a_prefix)

    def new_context(self, prefix, title=None, search_path=None, owner = None) -> str:
        return new_context(self.session_id, prefix, title, search_path, owner)

    def newConcept(self, preciseTerm, **kwargs) -> PID:
        return newConcept(self.session_id, preciseTerm, **kwargs)

    def getConcept(self, identity, base=None) -> ConceptComm:
        ret = getConcept(self.session_id, identity, base)
        if not self.raw_data:
            ret['timestamp'] = format.timestamp_deserialise(ret['timestamp'])
        return ret

    def newInst(self, cls, name, **extra) -> PID:
        return newInst(self.session_id, cls, name, **extra)

    def getInst(self, identity):
        ret = getInst(self.session_id, identity)
        if not self.raw_data:
            ret['timestamp'] = format.timestamp_deserialise(ret['timestamp'])
        return ret

    def get(self, identity):
        ret = get(self.session_id, identity)
        if not self.raw_data:
            ret['timestamp'] = format.timestamp_deserialise(ret['timestamp'])
        return ret

    def status(self):
        return status(self.session_id)
    
    def context_status(self, a_prefix=None):
        return context_status(self.session_id, a_prefix)

    def find(self, query, pid_only=False, only_these=None):
        return find(self.session_id, query, pid_only, only_these)

