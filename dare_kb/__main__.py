#!/usr/bin/env python3
# -*- coding:utf-8 -*-

from .server import service

import logging
logging.basicConfig(level=logging.DEBUG)


def main():
    service.run()


if __name__ == '__main__':
    main()
