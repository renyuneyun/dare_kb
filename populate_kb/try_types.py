# an experiment to understand and prototype type annotations useful in DKB functions and documentation
# NB, yet to be agreed and adopted.

from typing import Any, Union, List, Dict, FrozenSet, Tuple

PID = str                   # DKB allocated PID for an instance
Name = str                  ## user's local name for an instance, resolved by a search, in the current Context first then follow search paths
Name_Prefix = str           # Name:Prefix identifies the instance with that name in that Context. Separator may change
Id = Union[ Name, Name_Prefix, PID ]     # identifies an instance
AttrId = str                # identifies an attribute within an instance
AttrDict = Dict[ AttrId, Any ]            # representation of set of attributes and their values
Combiner = FrozenSet[str]   #  { 'AND', 'OR', 'NOT' } # only AND currently implemented
Cond1 = Tuple[ str, Id ]                  # a condition, operator str = 'isa' or 'isa_exactly' followed by value
Cond2 = Tuple[ str, AttrId, Any ]         # a condition as operator str = '==', attribute, value
Query = List[ Union[ Combiner, Cond1, Cond2 ] ]

def get(id: Id) -> AttrDict:
    return {'a': 1, 'b': 2}
    
def find( q: Query, pids_only: bool ) -> Union[ List[PID], List[AttrDict] ]:
    if pids_only:
        return ['aSite:pre:3:a', 'aSite:pre:4:b']
    else:
        return [{'a': 1, 'b': 2}, {'aa': 3, 'bb': 4}]

def new_inst( concept: Id, **attributes: AttrDict ) -> PID:
    return 'ex3KB:mpa:27:hen'

def test_types():
    a1 = get('a')
    a2 = get('a:mpa')
    a3 = get('ex3KB:mpa:27:hen')
    
    query = ['AND', ('==', 'a', 1), ('isa', 'Animal')]
    unit_animals = find(query, True)
    unit_animals2 = find(query, False)
    
    new_pid = new_inst( 'Animal', **{'aa': 3, 'bb': 4} )
    assert True, "Made it to the end"