# Python program to start populating kb with a definition of Concept AttributeSpec to define
# the permitted values of a Concept's attributes. It then makes an initial population of 
# instances of these to illustrate its use and to construct a Collection, attributeTypes, 
# held for now as a python dict mapping strings to AttributeSpec instances. 
# Eventually, this Collection will be used in Concept specifications and in 
# verifying that instances conform to those definitions.

import dare_kb.DKBlib as DKB
import sys, getopt

_verbose = False
_site = 'ex3KB'         # which site we're populating kb for
_user = 'mpa'           # who is running the population generator

dkb = None

def vprint(s: str):
    if _verbose:
        print(s)

def getCLparams():      # if CL parameters set override the defaults already set
    global _verbose, _site, _user
    try:
        opts, args = getopt.getopt(sys.arg[1:],"hvs:u:",["site=","user="])
    except getopt.GetoptError:
        print( 'mkAttributeSpec.py -v -s <site> -u <user>' )
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('''-h help
            -v verbose
            -s, --site = str installation identifier
            -u, --user = str user identifier''')
        elif opt == '-v':
            _verbose = True
        elif opt in ('-s', '--site'):
            _site = arg
        elif opt in ('-u', '--user'):
            _user = arg
    vprint( '_verbose = ' + str(_verbose) + 
            '\n_site = ' + _site +
            '\n_user = ' + _user)

Attribute_spec = None
def spec_Attribute_spec():
    global Attribute_spec
    pass

def makeAttributeSpecs():
	pass
	
def main():
    global dkb
    getCLparams()        # obtain any defaults overridden on the Command Line
    dkb = DKB.login(_site, _user)
    dkb.enter('kb', 'W')
    spec_Attribute_spec()  # specify the Concept Attribute_spec
    make_attribute_specs() # generate a population of 'standard' attribute specs
    dkb.close()
    
if __name__=='__main__':     
    main()
 