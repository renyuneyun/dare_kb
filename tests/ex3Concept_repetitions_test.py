# an ex3 test of the capacity of DKB to cope with a significant volume of work and a
# measure of its rate of doing work , by assessing the time it takes to do a mix of things

import DKBlib as DKB
import pytest
from DKBlib import Exceptions
from time import process_time
import datetime as dt
import random
import pickle
import os

_attributes_working = True
_state_settable = True
_extra_working = False             # unspecified properties are not permitted in instances
_find_working = True

_verbose = True
_reps = 5						# number of times whole workload is repeated
_prefix_start = 'prefixbase'    # basis for that many Contexts
_number_of_Concepts = 10        # each of which will specify this many concepts + those from tests
_instance_pop = 10              # number of instances per Concept
_get_count = 5                  # number of find pattern runs per major iteration
_time_threshold = 5             # maximum permitted time in seconds to complete one cycle of the workload
_indent = 4*' '
_separator = ':'

mt_dic = {}     # many occasions an empty dictionary is needed
                # offsets to unpack a remembered Concept 
_name = 0		# index for name
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by newConcept
_descr = 7		# the description of the Concept

_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept in ex3KB installation
dkb = None		

def vprint( s: str):
    if _verbose:
        print(s)

_line_width = 100
_sep = ', '
_sep_len = len(_sep)
def vprint_mc( hdr: str, body):
    if not _verbose:
        return
    used = len(hdr) + 3
    line = hdr + ' '
    first = True
    if type(body) == list:
        line += '[ '
        for nxt in body:
            if first:
                nxt_item = str(nxt)
                first = False
            else:
                nxt_item = _sep + str(nxt)
            uses = len(nxt_item)
            if used + uses > _line_width:
                print(line)
                used = uses
                line = nxt_item
            else:
                line += nxt_item
                used += uses
        line += ' ]'
        print(line)
    elif type(body) == dict:
        line += '{ '
        for nxt in body:
            val = str(body[nxt])
            nxt_item = str(nxt) + ': ' + val
            if first:
                first = False
            else:
                nxt_item = _sep + nxt_item
            uses = len(nxt_item)
            if used + uses > _line_width:
                print(line)
                used = uses
                line = nxt_item
            else:
                line += nxt_item
                used += uses
        line += ' }'
        print(line)
    else:
        print(line + ' body is ' + str(type(body)))
        print(str(body))
        
        
def eqDic(dic1: dict, dic2:dict,     # true iff the two supplied dictionaries are equal
          name:str) -> bool:
    assert len(dic1) == len(dic2), 'The two dictionaries, \'' + str(dic1) + '\' and \'' + \
                      str(dic2) + '\' have different lengths for ' + name 
    if len(dic1) != len(dic2):
        return False
    for key in dic1:
        assert key in dic2, '\'' + key + '\' in ' + str(dic1) + \
                        ' but not in ' + str(dic2)
        if not key in dic2:
            return False
        assert dic1[key] == dic2[key], 'The values for \'' + key + '\' differ' 
        if dic1[key] != dic2[key]:
            return False
    return True
  
def set_controls():             # override any of the default controls from the command line
    global _reps, _prefix_start, _number_of_Concepts, _instance_pop, _get_count
    pass                        # to be implemented  				

def isaConcept(res):            # given a dictionary returned by a get test if it is
                                # an instance of a Concept
    return res['instanceOf'] == _ConceptPID

def getResultAsExpected( res,  # test whether the results, res, returned by get
          tup,                 # meet the expectations recorded in tup for a Concept
          i):                  # the iteration number for errors and prefix
    # test every returned attribute as far as possible
    the_prefix = _prefix_start + str(i)
    assert isaConcept(res), 'Not a Concept because \'instanceOf\' != Concept Concept PID'
    assert 'name' in res, 'Attribute name missing'
    assert res['name'] == tup[_name], 'name = \'' + res['name'] + \
           '\' it should be \'' + tup[_name] + '\''
    assert 'specialisationOf' in res, 'Attribute specialisationOf missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['specialisationOf'] == tup[_super], 'It specialises \'' + res['specialisationOf'] + \
           '\' whereas it should specialise \'' + tup[_super] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'instanceOf' in res, 'Attribute instanceOf missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['instanceOf'] == _ConceptPID, 'It is an instance of \'' + res['instanceOf'] + \
           '\' whereas it should an instance of Concept, i.e., \''  + _ConceptPID + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'prefix' in res, 'Attribute prefix missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['prefix'] == the_prefix, 'It has prefix \'' + res['prefix'] + \
           '\' whereas it should say it was made in \'' + the_prefix + '\'' + \
           ' for \'' + tup[_name] + '\' in iteration' + str(i)
    assert 'pid' in res, 'Attribute pid missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['pid'] == tup[_pid], 'PID \'' + res['pid'] + '\' not \'' + tup[_pid] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'state' in res, 'Attribute state missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['state'] == 'new', 'state \'' + res['state'] + '\' is not \'' + 'new' + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'timestamp' in res, 'No timestamp returned' + \
           ' for \'' + tup[_name] + '\''
    assert isinstance(res['timestamp'], dt.datetime), "timestamp not a datetime" + \
           ' for \'' + tup[_name] + '\''
    assert 'extra' in res, 'Attribute extra missing' + \
           ' for \'' + tup[_name] + '\''
    if _extra_working:
        assert eqDic(res['extra'], mt_dic, tup[_name]), 'Property \'extra\' is not an empty dictionary' + \
           ' for \'' + tup[_name] + '\''
    assert 'mutability' in res, 'Attribute mutability missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['mutability'] == 'mutable', 'Attribute \'mutable\' not set tp \'mutable\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'required' in res, 'Attribute required missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['required'], tup[_reqs], tup[_name] + '.required'), 'required is ' + str(res['required']) + \
                    ' but it should be ' + str(tup[_reqs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'recommended' in res, 'Attribute recommended missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['recommended'], tup[_recs], tup[_name] + '.recommended'), 'recommended is ' + str(res['recommended']) + \
                    ' but it should be ' + str(tup[_recs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'optional' in res, 'Attribute optional missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['optional'], tup[_opts], tup[_name] + '.optional'), 'optional is ' + str(res['optional']) + \
                    ' but it should be ' + str(tup[_opts]) + \
           ' for \'' + tup[_name] + '\''
    assert 'translation' in res, 'Attribute translation missing' + \
           ' for \'' + tup[_name] + '\''
   # assert eqDic(res['translation'], tup[_trans], tup[_name]), 'translation is ' + str(res['translation']) + \
   #                 ' but it should be ' + str(tup[_trans])
    assert 'description' in res, 'Attribute description missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['description'] == tup[_descr], 'description is \n' + str(res['description']) + \
                    '\nbut it should be\n' + str(tup[_descr]) + \
           ' for \'' + tup[_name] + '\''
    return True
    
_newConceptsDict = dict()		# a dictionary mapping names to tuples
def rememberConcept(
  nom: str,						# string its user-supplied name
  superPID: str,			    # the PID of the Concept it specialises
  reqDic: dict,					# a dictionary of the required name: Concept
  recDic: dict,					# a dictionary of the recommended attributes
  optionalDic: dict, 			# a dictionary of the optional attributes
  translation: dict,            # a dictionary of attribute name translations
  pid: str,						# the identifier returned by newConcept
  descr: str = '',              # the description if there is one
  ):
    global _newConceptsLDict   	# accumulate 8-tuples there
    tup = (nom, superPID, reqDic, recDic, optionalDic, translation, pid, descr)
    _newConceptsDict.update({nom: tup})
    
def set_prefix_start():
    global _prefix_start
    while True:
        try: 
            dkb.enter(_prefix_start + str(0), 'R')
            _prefix_start = 'z' + _prefix_start  # if enter succeeds already used
            continue
        except Exceptions.ContextNotFoundError:  # we are trying to get here!
            return              # it has not been used before

_a_name = 'Con'              # start of Concept's name
_cons = []                   # array of Concepts
_Concept_names = []          # names of Concepts
_per_iteration = dict()          # hold here each iteration's testable state

def spec_Concepts(i: int):       # specify the Concepts again in this Context
    for k in range(_number_of_Concepts):
        nom = _a_name + k*'x'
        _Concept_names.append(nom)
        descr = "Test load Concept " + nom + " in iteration " + str(i)
        req_attribs = {}
        for j in range(k):
            req_attribs.update({'attrib' + j*'y': 
                                'String' if j&1 else 'Integer'})
        vprint('newConcept( ' + nom + ', ' + str(req_attribs) + ', ' + descr + ' )' )
        con = dkb.newConcept( nom,
                      required = req_attribs,
                      description = descr )
        _cons.append(con)
        rememberConcept(nom, _ConceptPID, req_attribs, mt_dic, mt_dic, mt_dic, con, descr)

def get_Concepts(i: int):
    for j in range(_get_count):  # repeat to get reading to writing ratio realistic
        for k in range(_number_of_Concepts):
            by_name = dkb.get(_Concept_names[k])
            by_pid = dkb.get(_cons[k])
            assert getResultAsExpected( by_name, _newConceptsDict[_Concept_names[k]], i), "by_name result wrong for k = " + \
                                                    str(k) + " at iteration " + str(i)
            assert getResultAsExpected( by_pid, _newConceptsDict[_Concept_names[k]], i), "by_name result wrong for k = " + \
                                                    str(k) + " at iteration " + str(i)

_strings = ['a', 'serendipity', 'Kathy Zuckerman', 'Tamzyn Macfarlane', "Yarner Atkinson Annear",
            'b' 'James Bond', 'Meryl Streep', 'Rui Zhao', 'Yuexuan Li', 'Aurora Constantin',
            'c', 'Rosa Filgueira', 'Amélie Levray', 'Kirsteen Elizabeth Morwenna Atkinson Annear',
            'd', 'Janna Mairi Gordon Atkinson', 'Lila Guthrie', 'Moya Guthrie']
_str_len = len(_strings)
def choose_str(i: int) -> str:
    return _strings[i]
 
_states = ['new', 'updated', 'valid', 'authoritative', 'depricated', 'discarded']        
def choose_state() -> str:     # chooses a possible state
    return _states[random.randint(0, len(_states) - 1)]

_mutabilities = ['mutable', 'no hjiding', 'frozen']
def choose_mutability() -> str:
    return _mutabilities[random.randint(0, len(_mutabilities) - 1)]

def get_name_from_pid(pid: str) -> str:
    _num_seps = pid.count(_separator)
    assert _num_seps == 3, "malformed pid"
    pid_parts = pid.split( sep = _separator, maxsplit = 4)
    return pid_parts[3]
    
def print_instance_creation(
    inst_name: str, 
    Concept_pid: str, 
    new_inst_pid: str,
    properties: dict,
    more_properties: dict     # not included in properties and stored instance if NOT _attribute_working
    ):
    concept_name = get_name_from_pid(Concept_pid)
    print( inst_name + ' = ' +concept_name + '(' )
    for prop in properties:
        print( _indent + prop + ' = ' + str(properties[prop]) )
    if not _attributes_working:
        print('omitted')
        for prop in more_properties:
            print( _indent + prop + ' = '  +str(more_properties[prop]) )
    print( _indent + ')' )
   
_inst_dic = dict()
def populate_Concepts(i: int):
    for k in range(_number_of_Concepts):
        name = _Concept_names[k]
        pid = _cons[k]
        required = _newConceptsDict[name][_reqs]
        inst_id_base = 'a' + name + '_' + str(i) + '_' + str(k) + '_'
        for inst_no in range(_instance_pop):
            inst_name = inst_id_base + str(inst_no)
            attributes = {'name': inst_name}
            properties = {}                  # collect here ones we can't set at the moment
            _state_set = False
            for attr in required:
                choose_no = (i*k*inst_no + random.randint(0, 10000)) % _str_len
                value = choose_no if required[attr] == 'Integer' else choose_str(choose_no)
                properties.update({attr: value})   
            # add some variation testable without attributes in get and find
            if _state_settable and random.randint(0,10) > 8:
                universal_properties = {'state': choose_state(), 
                                        'mutability': choose_mutability()}
                properties.update(universal_properties)
                _state_set = True
            if _attributes_working:
                attributes.update(properties)
            newInstPID = dkb.newInst(pid, **attributes)
            _other_attributes2check = {
                                       'instanceOf': pid}
            if not _state_set:
                _other_attributes2check.update({'pid': newInstPID, 'state': 'new', 
                                       'prefix': _prefix_start + str(i)})
            attributes.update(_other_attributes2check)
            _inst_dic.update({inst_name: attributes})
            if _verbose:
                print_instance_creation( inst_name,
                                         pid,
                                         newInstPID,
                                         attributes,   # the ones used
                                         properties )  # would be included if _attributes_working

def get_instances(i: int):
    the_prefix = _prefix_start + str(i)
    for j in range(_get_count):  # repeat to get reading to writing ratio realistic
        for name in _inst_dic:
            res = dkb.get(name)
            print('res = dkb.get(' + name + ')')
            assert res['name'] == name, 'Should be called ' + name
            assert res['prefix'] == the_prefix, 'prefix not set to ' + the_prefix + ' for ' + name
            expected = _inst_dic[name]       # stored dictionary of assigned properties
            for attr in expected:            # assume automated properties are OK
                expected_value = expected[attr]
                assert attr in res, 'Attribute \'' + attr +'\' expected but not found for ' + \
                                    name + " during iteration " + str(i)
                assert res[attr] == expected_value, 'Result of get( ' + \
                                            name + ' ) for property ' + attr + \
                                            ' does not match: retrieved = ' + \
                                            str(res[attr]) + ' expected = ' + \
                                            str(expected_value) + ' in get try ' + str(j) + \
                                            ' for workload iteration ' + str(i)

def find_instances(i: int):
    the_prefix = _prefix_start + str(i)
    if not _find_working:
        return
    for j in range(_get_count):  # repeat to get reading to writing ratio realistic
        for nom in _inst_dic:
            attributes = _inst_dic[nom]
            query = ['AND', ('==', 'prefix', the_prefix)]
            vprint('Trying dkb.find(' + query + ' in iteration ' + str(i))
            res = dkb.find(query)  # all instances in this Context
            vprint_mc('res = ', res)
            assert len(res) == _number_of_Concepts * (1 + _instance_pop), 'Number of instances in this Context wrong iteration ' + str(i)
            isa_Concept = attributes['instanceOf']
            query = ['AND', ('==', 'prefix', the_prefix), ('isa', isa_Concept)]
            res = dkb.find(query)
            vprint_mc('Result for ' + str(query), res)
            assert len(res) == _instance_pop, 'Number of instances of ' + isaConcept + \
                                              'not the expected ' + str(_instance_pop) +\
                                              'in Context ' + the_prefix + ' for iteration ' + str(i)
            query = ['AND', ('==', 'prefix', the_prefix), ('isa_exactly', isa_Concept)]
            res = dkb.find(query)
            vprint_mc('Result for ' + str(query), res)
            assert len(res) == _instance_pop, 'Number of exact instances of ' + isaConcept + \
                                              'not the expected ' + str(_instance_pop) +\
                                              'in Context ' + the_prefix + ' for iteration ' + str(i)
            query = ['AND', ('==', 'prefix', the_prefix), ('==', 'name', nom)]
            res = dkb.find(query)
            vprint_mc('Result for ' + str(query), res)
            assert len(res) == 1, 'Number of instances with name == ' + nom + \
                                              'not the expected 1 in Context ' + the_prefix + ' for iteration ' + str(i)
            query = ['AND', ('==', 'name', nom)]
            res = dkb.find(query)
            vprint_mc('Result for ' + str(query), res)
            assert len(res) >= i, 'There should be one with each name in each Context' + ' for iteration ' + str(i)

_iter_no = 0
_iter_Concept_names = 1
_iter_inst_dic = 2
_iter_prefix = 3
_iter_cons = 4
def remember_iteration(i: int):
    global _Concept_names, _inst_dic, _cons
    tup = (i, _Concept_names, _inst_dic, _prefix_start + str(i), _cons)
    _per_iteration.update({i: tup})     # may not be needed but may pickle for future use
    _Concept_names = []                 # avoid overlap with previous iterates
    _inst_dic = dict()
    _cons = []      

_pickling_dir = '../../pickles/'        # place where pickle files are stored 
_file_name = 'repetition_state.pkl'   
def save_state():                       # pickle state accumulated by remember_iteration
    if not os.path.isdir(_pickling_dir):
        print( 'Directory for pickling ' + _pickling_dir + ' not found; state not saved')
        return
    pickle_path = _pickling_dir + _file_name
    if os.path.isfile(pickle_path):
        print( 'overwritting previous pickle in ' + pickle_path )
        os.remove(pickle_path)
    with open(pickle_path, 'wb') as output:
        pickle.dump(_per_iteration, output, pickle.HIGHEST_PROTOCOL)
        output.close()
    

def test_workload():
    global dkb
    set_controls()
    start_time = process_time()
    dkb = DKB.login('ex3KB',
            userName = 'mpa')     
    # work in the mpa Context
    dkb.enter('mpa', 'W')
    set_prefix_start()
    for i in range(_reps):
        the_prefix = _prefix_start + str(i)
        vprint("the_prefix = " + the_prefix)
        a_context = dkb.newContext( 'workload test rep ' + str(i),
                                    the_prefix )
        vprint("the_prefix = " + the_prefix + " a_context = " + a_context)
        assert the_prefix == a_context, "Result " + a_context + \
              "does not match requested Context name" + the_prefix + \
              " at iteration " + str(i)
        dkb.enter( the_prefix, 'W')      # work here for 1 iteration
        spec_Concepts(i)
        get_Concepts(i)
        populate_Concepts(i)
        get_instances(i)
        find_instances(i)
        remember_iteration(i)
        dkb.leave()
        dkb.enter('mpa', 'W')
    dkb.close()
    end_time = process_time()
    elapsed_time = end_time - start_time
    unit_time = elapsed_time / _reps
    print('Elapsed time = ' + str(elapsed_time) + ' unit time = ' + str(unit_time) +
          ' for ' + str(_reps) + ' repetitions')
    assert unit_time < _time_threshold   # currently set too generously for debug
    save_state()                         # pickle for future use
    
    