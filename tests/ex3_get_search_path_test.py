# Ex3 pytest file 
# Test function get_search_path
 
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import datetime

dkb = None


def eq_list(list1: list, list2: list) -> bool:
    'Return True iff the two arguments are identical lists'
    assert isinstance(list1,list), 'value returned by get_search_path is not a list'
    if not isinstance(list1,list):
       return False
    assert len(list1) == len(list2),\
        'search path not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if len(list1) != len(list2):
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        assert isinstance(elem1, str),\
            'search path contains a non-string: ' + str(elem1)
        if not isinstance(elem1, str):
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        assert elem1 == elem2,\
        	'Mismatch of orefixes at step ' + str(posn) +\
                        ' ' + elem2 + ' expected but ' + elem1 + ' found'
        if elem1 != elem2:
            return False
        posn += 1
    return True
    
def test_get_search_path():
    global dkb
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now())) 
      
    # Make some contexts to do the test with
    # Make certain not in a Context
    #dkb.leave()    # leave the Context retained from the last login
    con1 = dkb.new_context( 'mpa2', title = 'ex3 testing get_search_path' )      # pour moi
    
    con2 = dkb.new_context( 'mal2', # pour moi aussi
                title = 'More ex3 testing get_search_path',
                search_path = ['mpa2'] ) # with an explicit search path
    
    con3 = dkb.new_context( 'malcolm3',             # pour moi encore
                title = 'With explicit owner ex3 testing get_search_path',
                search_path = ['mpa2', 'mal2'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    
    # check that the search paths are as expected
    # this also checks get_search_path
    dkb.enter('mpa', 'R')             #from previous run
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa'
    assert eq_list(sp1, ['kb']), '''mpa\'s search path not [\'kb\']'''
    
    dkb.enter('mpa2', 'R')            #test enter in ex3entertest.py
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa'
    assert eq_list(sp1, ['mal7']), '''mpa2\'s search path not [\'mal7\'] prevailing at close of ex3_leave_test.py'''
    
    dkb.enter('mal2', 'R')            #check mal's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal'
    assert eq_list(sp2, ['mpa2']), '''mal2\'s search path not [\'mpa2\']'''
    
    dkb.enter('malcolm3', 'R')        #check malcolm's search path
    sp3 = dkb.get_search_path()		  # get search_path for Context 'malcolm3'
    assert eq_list(sp3, ['mpa2', 'mal2']), '''malcolm3\'s search path not [\'mpa2\', \'mal2']'''
   
def test_get_search_path_exception_raising():
    dkb.close()	#finished this test
    with pytest.raises(Exceptions.DKBclosedError):           
        con11 = dkb.get_search_path()


