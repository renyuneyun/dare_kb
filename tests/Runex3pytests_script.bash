echo +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++run start+++++++++
# assumed to be running in the tests subdirectory 
rm ex3KB.sqlite3            # need a reset with client-server model
ls ex3*
rm ../../tmp_files/*
rm ex3KB.sqlite3
rm ../../pickles/*

# start DKB instance testing 
pytest ex3newDKB_test.py
pytest ex3login_test.py
pytest -s ex3status_test.py

pytest ex3newContext_test.py
pytest ex3getSearchpath_test.py
pytest ex3setSearchpath_test.py
pytest ex3enter_test.py
pytest ex3leave_test.py
pytest ex3retaint_test.py
pytest ex3contextRepetitions_test.py
pytest -s ex3status_test.py
            # fns on contexts not marked as future work 
# pytest contextStatus_test.py   # fn to be implemented test to be written
# pytest freeze_test.py   # fn to be implemented test to be written

# completed Context testing


# start Concept testing
pytest ex3newConcept_test.py
pytest ex3get_Concepts_test.py
pytest -s ex3newInstance_test.py > ../../tmp_files/new_instance.log
pytest ex3get_inst_test.py
pytest -s ex3Concept_repetitions_test.py > ../../tmp_files/repetitions.log
pytest -s ex3status_test.py > ../../tmp_files/status3.log
exit
pytest ex3find1_test.py                  # yet to be completed
pytest ex3get_onlyThese_test.py          # yet to be tested
pytest ex3get_inst_onlyThese_test.py     # yet to be written
# update both Concepts and instances     # all yet to be written
pytest ex3update_test.py
# verify updates worked
pytest ex3get_updates_test.py
# test a broad set of search options
pytest ex3find_test.py

# completed Concept testing

# start Concept in Contexts testing

