# Ex3 pytest tests
# Test function enter
 
import DKBlib as DKB
import pytest 
from DKBlib import Exceptions
import datetime

dkb = None

def eq_list(list1: list, list2: list) -> bool:
    'Return True iff the two arguments are identical lists'
    assert isinstance(list1,list), 'value returned by get_search_path is not a list'
    if not isinstance(list1,list):
       return False
    assert len(list1) == len(list2),\
        'search path not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if len(list1) != len(list2):
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        assert isinstance(elem1, str),\
            'search path contains a non-string: ' + str(elem1)
        if not isinstance(elem1, str):
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        assert elem1 == elem2,\
        	'Mismatch of orefixes at step ' + str(posn) +\
                        ' ' + elem2 + ' expected but ' + elem1 + ' found'
        if elem1 != elem2:
            return False
        posn += 1
    return True
    
    
def test_enter():
    global dkb
    dkb = DKB.login('ex3KB',
            'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now()))       
    # Make some contexts to do the test with
    con1 = dkb.new_context( 'mpa5', title = 'ex3 testing for enter' )      # pour moi
    
    con2 = dkb.new_context( 'mal5', title = 'More ex3 testing for enter', # pour moi aussi
                search_path = ['mpa5'] ) # with an explicit search path
    
    con3 = dkb.new_context( 'malcolm5',             # pour moi encore
                title = 'With explicit owner ex3 testing for enter',
                search_path = ['mpa5', 'mal5'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    
    # check that the Contexts can be entered
    # this checks set_search_path
    dkb.enter('mpa5', 'W')            #test enter in a different program, assume OK here
    dkb.set_search_path( ['malcolm5'] ) # set search_path for Context 'mpa5'
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa5'
    assert eq_list(sp1, ['malcolm5']), '''mpa5\'s search path not [\'malcolm5\']'''
    
    dkb.enter('mal5', 'R')            # set mal4's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal5'
    assert eq_list(sp2, ['mpa5']), '''mal5\'s search path not [\'mpa5\']'''
    
    dkb.enter('malcolm5', 'R')        #check malcolm5's search path
    sp3 = dkb.get_search_path()		  
    assert eq_list(sp3, ['mpa5', 'mal5']), '''malcolm4\'s search path not [\'mpa5\', \'mal5\']'''
    
    # try a revisit in a different mode
    dkb.enter('mpa5', 'R')            #test enter in a different program, assume OK here
    sp4 = dkb.get_search_path()		  # get search_path for Context 'mpa5'
    assert eq_list(sp4, ['malcolm5']),'''mpa5\'s search path not [\'malcolm5\']'''
    dkb.enter('mpa5', 'W')            # same Context different mode
    sp4 = dkb.get_search_path()		  # get search_path for Context 'mpa5'
    assert eq_list(sp4, ['malcolm5']),'''mpa5\'s search path not [\'malcolm5\']'''

def test_enterExceptions():
    with pytest.raises(Exceptions.ContextNotFoundError):
        dkb.enter( 'nemo', 'R' )	  # cause ContextNotFoundError
    with pytest.raises(TypeError):
        dkb.enter( 'mpa5', 'X' )	  # Not R or W causes TypeError!
    dkb.close()
    with pytest.raises(Exceptions.DKBclosedError):
        dkb.enter( 'malcolm5', 'R' )	# cause DKBclosedError


