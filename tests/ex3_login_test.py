#Ex3 
# Test function to login to a DKB instance
# It also tests close
 
import DKBlib as DKB
from DKBlib import Exceptions
import pytest
import datetime

dkb = None

def test_minimalLogin():
    global dkb
    # login to a DKB in its current state state
    dkb = DKB.login('ex3KB', 'Nemo')
    assert dkb != None				# a result should be returned
    assert isinstance(dkb, DKB.DKBService) # instance of client-side root fo KB

def test_close(): 
    dkb.close()

def test_login_again():
    # check can be opened with explicit user name
    dkb = DKB.login( 'ex3KB',      # Second time with user name explicit
            'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now()))
    assert dkb != None				# a result should be returned
    assert isinstance(dkb, DKB.DKBService) # instance of client-side root fo KB    
    dkb.close()

def test_NotFound():
    global dkb   
    # check for anticipated exceptions
    with pytest.raises(Exceptions.DKBnotFoundError):
        DKB.login('ex3KB_that_does_not_exist', 'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now()) )
    