#!/bin/sh

rm ex3*sqlite3
ls ex3*

pytest ex3_login_test.py ex3_status_test.py
# completed DKB instance testing

# start context testing
pytest ex3_new_context_test.py ex3_enter_test.py ex3_leave_test.py ex3_get_search_path_test.py ex3_set_search_path_test.py 
pytest ex3_retained_test.py ex3_context_repetitions_test.py ex3_status_test.py
# completed Context testing

# start Concept testing
pytest ex3newConcept_test.py
pytest ex3get_Concepts_test.py
pytest ex3newInstance_test.py
pytest ex3get_inst_test.py
pytest ex3find1_test.py
pytest -s ex3Concept_repetitions_test.py
