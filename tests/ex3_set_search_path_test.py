# Ex3 pytest file 
# Test function set_search_path
 
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import logging
import os
import datetime

dkb = None


def eq_list(list1: list, list2: list) -> bool:
    'Return True iff the two arguments are identical lists'
    assert isinstance(list1,list), 'value returned by get_search_path is not a list'
    if not isinstance(list1,list):
       return False
    assert len(list1) == len(list2),\
        'search path not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if len(list1) != len(list2):
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        assert isinstance(elem1, str),\
            'search path contains a non-string: ' + str(elem1)
        if not isinstance(elem1, str):
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        assert elem1 == elem2,\
        	'Mismatch of orefixes at step ' + str(posn) +\
                        ' ' + elem2 + ' expected but ' + elem1 + ' found'
        if elem1 != elem2:
            return False
        posn += 1
    return True
       
    
def test_set_search_path():
    global dkb
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now())) 
      
    # Make some contexts to do the test with
    con1 = dkb.new_context( 'mpa4', title = 'ex3 testing set_search_path')      # pour moi
    
    con2 = dkb.new_context( 'mal4', # pour moi aussi
                title = 'More ex3 testing set_search_path',
                search_path = ['mpa4'] ) # with an explicit search path
    
    con3 = dkb.new_context( 'malcolm4',             # pour moi encore
                title = 'With explicit owner ex3 testing set_search_path',
                search_path = ['mpa4', 'mal4'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    
    # check that the search paths are as expected
    # this checks set_search_path
    dkb.enter('mpa4', 'W')            #test enter in a different program, assume OK here
    dkb.set_search_path( ['malcolm4'] ) # set search_path for Context 'mpa4'
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa4'
    assert eq_list(sp1, ['malcolm4']), '''mpa2\'s search path not [\'malcolm4\']'''
    
    dkb.enter('mal4', 'W')            # set mal4's search path
    dkb.set_search_path(['kb', 'malcolm4']) # set search_path for Context 'mal4'
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal4'
    assert eq_list(sp2, ['kb', 'malcolm4']), '''mal4\'s search path not [\'kb\', \'malcolm4'\']'''
    
    dkb.enter('malcolm4', 'W')        #check malcolm4's search path
    dkb.set_search_path([])		      # set empty search path for Context 'malcolm4'
    sp3 = dkb.get_search_path()		  # set malcolm4 to only look locally
    assert eq_list(sp3, []), '''malcolm4\'s search path not []'''
   
def test_set_search_path_exceptions():
    dkb.enter('mpa', 'W')
    with pytest.raises(Exceptions.ContextNotFoundError):
        dkb.set_search_path( ['nemo'] ) # cause ContextNotFoundError
    with pytest.raises(Exceptions.WritingPermissionDeniedError):
        dkb.enter('mal4', 'R')
        dkb.set_search_path( ['malcolm4'] )
    dkb.close()	
    with pytest.raises(Exceptions.DKBclosedError):
        sp4 = dkb.set_search_path( ['malcolm4'] )	# cause DKBclosedError


