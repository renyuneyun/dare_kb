#!/bin/sh

rm ex3*sqlite3
ls ex3*

pytest ex3login_test.py ex3status_test.py
# completed DKB instance testing

# start context testing
pytest ex3newContext_test.py ex3getSearchpath_test.py ex3setSearchpath_test.py ex3enter_test.py ex3leave_test.py ex3retaint_test.py ex3status_test.py
# completed Context testing

# start Concept testing
pytest ex3newConcept_test.py
pytest ex3get_Concepts_test.py
pytest ex3newInstance_test.py
pytest ex3get_inst_test.py
pytest ex3find1_test.py
pytest -s ex3Concept_repetitions_test.py

# Rui's old lines, probably remove but is && significant?
pytest ex3newConcept_test.py &&
pytest ex3get_Concepts_test.py &&

pytest ex3newInstance_test.py &&
pytest ex3get_inst_test.py

