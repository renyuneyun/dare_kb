# Ex3 Pytset tests
# Test function get obtaining Concepts with onlyThese set to obtain projections
# for the simple examples in one Context (mpa) made by ex3newConcepttest
 
import DKBlib as DKB
import pytest 
from DKBlib import Exceptions
import datetime as dt
import sys 
import pickle
import random 

_picklingDir = '../../pickles/'     # place where pickle files are stored
_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept
_verbose = False
mtDic = {}               # many occasions an empty dictionary is needed
    
_name = 0		# index for name
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by newConcept
_descr = 7		# the description of the Concept

# The attributes every entry in the DKB has automatically
_builtInAttributes = ['pid', 'prefix', 'instanceOf', 'name', 'timestamp', 'state']
_conceptAttributes = ['name', 'prefix', 'pid', 'timestamp', 'instanceOf', 'state',
                       'predecessor', 'successor', 'mutability', 'extra', 
                       'instanceOf', 'description', 'required', 'recommended',
  #                     'optional', 'translation']
                       'optional', 'translation', 'methods', 'class']

dkb = None						# global shared DKB instance variable set at start

    # save a record of Concepts created in a form that is easily pickled and restored
_newConceptsDict = dict()		# a dictionary mapping names to tuples

_alreadyRestored = False
def restoreRememberedConcepts(): # recover from the file made by saveRememberedConcepts
    global _newConceptsDict, _alreadyRestored
    if _alreadyRestored:
        return
    _alreadyRestored = True
    with open(_picklingDir + 'mpasConcepts.pkl', 'rb') as input:
        _newConceptsDict = pickle.load(input)

def printDict(dic: dict,
            indent: str = ''):	# print the supplied dictionary 
    for k in dic:
        print(indent + 
              k + ': ' + str(dic[k]))
              
def printGetResult(res: str):   #print what is returned by get, irrespective of what it is
    if not _verbose:
        return
    for k in res:
        print( k + ': ' + str(res[k]))
               
def eqDic(dic1: dict, dic2: dict):   # true iff the two supplied dictionaries are equal
    assert len(dic1) == len(dic2), 'The two dictionaries, \'' + str(dic1) + '\' and \'' + \
                      str(dic2) + '\' have different lengths' 
    if len(dic1) != len(dic2):
        return False
    for key in dic1:
        assert key in dic2, '\'' + key + '\' in ' + str(dic1) + \
                        ' but not in ' + str(dic2)
        if not key in dic2:
            return False
        assert dic1[key] == dic2[key], 'The values for \'' + key + '\' differ' 
        if dic1[key] != dic2[key]:
            return False
    return True
    
def isaConcept(res):            # given a dictionary returned by a get test if it is
                                # a specialisation directly or indirectly of Concept
    return res['instanceOf'] == _ConceptPID
    
def isaSpecialisationOf(res,    # test whether the dictionary represents a 
                        aConcept): # directly or indirectly of the supplied Concept
    assert res.has_key('specialisationOf'), 'No specialisationOf attribute present => not a well-formed Concept'
    if not res.has_key('specialisationOf'):
        return False
    itSpecialised = res['specialisationOf']
    if itSpecialised == aConcept:
        return True
    if itSpecialised == _ConceptPID:  # ensure recursion terminates
        return False
    return isaSpecialisationOf( get(itSpecialised), # look a level further up
                                aConcept )

def vprintRememberedConcepts(): #if we're printing list the concepts we have made
    if not _verbose:
        return
    # here chatty
    _sep = '||\t'		# separate parts of a Concept at top level
    print('Created ' + str(len(_newConceptsDict)) + ' new concepts listed below')
    print('Parts: name' + _sep + 'superConcept' + _sep + 'required' + _sep + 
           'recommended' + _sep + 'optional' + _sep + 'translation' + 
           _sep + 'pid' + _sep + 'description')
    for key in _newConceptsDict:
        tup = _newConceptsDict[key]
        print( tup[_name] + _sep + 
                tup[_super] + _sep +
                str(tup[_reqs]) + _sep +
                str(tup[_recs]) + _sep + 
                str(tup[_opts]) + _sep + 
                str(tup[_trans]) + _sep + 
                str(tup[_pid]) + _sep +
                str(tup[_descr]))
    print('===\t\tEnd of Concept List\t\t===')
              
def printGetResult(res):   #print what is returned by get, irrespective of what it is
    if not isinstance(res, dict):
        errorReport('Not a dictionary as expected')
        return
    if not _verbose:
        return
    for k in res:
        print( k + ': ' +
               str(res[k]))

def printConcept(res):          # given a result returned by get, print it as a Concept
    assert isaConcept(res), 'Asking printConcept to print something that isn\'t a Concept'
    if not isaConcept(res):
        return
    print('\nComcept ' + res['name'] + ' created at time ' + str(res['timestamp']) +
        ' in Context ' + res['prefix'] + ' given PID ' + res['pid'] +
        ' with state = ' + res['state']) # start with standard attributes on a line
    indent = ' ' * 4
    specialises = res['specialisationOf']
    if specialises != _ConceptPID:
        print(indent + 'Specialises ' + specialises)
    extra = res['extra']
    if len(extra) > 0:	# extra is normally empty
        print(indent + 'extra = ' )
        printDict(extra, indent = ' ' * 8)
    if res['mutability'] == 'mutable':
        print(indent + 'It is mutable')
    else:
        print(indent + "mutability = " + res['mutability'])
    required = res['required']
    if len(required) > 0:       # are there required attributes
        printDict(required, indent = indent)
    recommended = res['recommended']
    if len(recommended) > 0:       # are there recommended attributes
        print(indent + '---')      # the indicator for recommended attributes follow
        printDict(recommended, indent = indent)
    optional = res['optional']
    if len(optional) > 0:          # are there optional attributes
        print(indent + '...')      # the indicator for optional attributes follow
        printDict(optional, indent = indent)
    translation = res['translation']
    if len(translation) > 0:       # are there translations of attribute ids?
        print(indent + 'translation =')      # the indicator for recommended attributes follow
        printDict(translation, indent = ' ' * 8)
    description = res['description']
    if description == None:
        return
    if description != '':
        print(indent + 'description =')
        print(description)         # no indent to finish off  
    

def getResultAsExpected( res,  # test whether the results, res, returned by get
          tup,                  # meet the expectations recorded in tup for a Concept
          onlyThese ):          # non-empty list of attribute names as strings
    # test every returned attribute as far as possible
    assert isinstance(onlyThese, list), 'Blunder testing code didn\'t supply list of strings'
    assert len(onlyThese) > 0, 'Blunder no items in supplied list'
    for s in onlyThese:
        assert isinstance(s, str), 'Blunder: item in supplied list not a string'
    assert len(onlyThese) == len(res), 'Number of returned items does not match requested number'
    if 'name' in OnlyThese:
        assert 'name' in res, 'Attribute name missing'
        assert res['name'] == tup[_name], 'name = \'' + res['name'] + '\' it should be \'' + tup[_name] + '\''
    if 'specialisationOf' in OnlyThese:
        assert 'specialisationOf' in res, 'Attribute specialisationOf missing'
        assert res['specialisationOf'] == tup[_super], 'It specialises \'' + res['specialisationOf'] + \
                                                        '\' whereas it should specialise \'' + tup[_super] + '\''
    if 'instanceOf' in OnlyThese:
        assert 'instanceOf' in res, 'Attribute instanceOf missing'
        assert res['instanceOf'] == _ConceptPID, 'It is an instance of \'' + res['instanceOf'] + \
                 '\' whereas it should an instance of Concept, i.e., \'' + _ConceptPID + '\''
    if 'prefix' in OnlyThese:
        assert 'prefix' in res, 'Attribute prefix missing'
        assert res['prefix'] == 'mpa', 'It has prefix \'' + res['prefix'] + \
               '\' whereas it should say it was made in \''   + 'mpa' + '\''
    if 'pid' in OnlyThese:
        assert 'pid' in res, 'Attribute pid missing'
        assert  res['pid'] == tup[_pid], 'PID \'' + res['prefix'] + '\' not \'' + tup[_pid] + '\''
    if 'state' in OnlyThese:
        assert 'state' in res, 'Attribute state missing'
        assert  res['state'] == 'new', 'state \'' + res['state'] +'\' is not \'' + 'new' + '\''
    if 'timestamp' in OnlyThese:
        assert 'timestamp' in res, 'No timestamp returned'
    if 'extra' in OnlyThese:
        assert 'extra' in res, 'Attribute extra missing'
        assert eqDic(res['extra'], mtDic), 'Property \'extra\' is not an empty dictionary'
    if 'mutability' in OnlyThese:
        assert 'mutability' in res, 'Attribute mutability missing'
        assert res['mutability'] == 'mutable', 'Attribute \'mutable\' not set tp \'mutable\''
    if 'required' in OnlyThese:
        assert 'required' in res, 'Attribute required missing'
        assert eqDic(res['required'], tup[_reqs]), 'required is ' + str(res['required']) + \
                    ' but it should be ' + str(tup[_reqs])
    if 'recommended' in OnlyThese:
        assert 'recommended' in res, 'Attribute recommended missing'
        assert eqDic(res['recommended'], tup[_recs]), 'recommended is ' + str(res['recommended']) + \
                    ' but it should be ' + str(tup[_recs])
    if 'optional' in OnlyThese:
        assert 'optional' in res, 'Attribute optional missing'
        assert eqDic(res['optional'], tup[_opts]), 'optional is ' + str(res['optional']) + \
                    ' but it should be ' + str(tup[_opts])
    if 'translation' in OnlyThese:
        assert 'translation' in res, 'Attribute translation missing'
        assert eqDic(res['translation'], tup[_trans]), 'translation is ' + str(res['translation']) + \
                    ' but it should be ' + str(tup[_trans])
    if 'description' in OnlyThese:
        assert 'description' in res, 'Attribute description missing'
        assert res['description'] == tup[_descr], 'description is \n' + str(res['description']) + \
                    '\nbut it should be\n' + str(tup[_descr])
    return True

def eqList(list1: list,list2:list) -> bool:
    'Return True iff the two arguments are identical lists'
    if not isinstance(list1,list):
        errorReport('value returned by getSearchpath is not a list')
        return False
    if len(list1) != len(list2):
        errorReport('search path not the length expected: returned = ' +
                    str(len(list1)) + ' but expected = ' +
                    str(len(list2)))
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        if not isinstance(elem1, str):
            errorReport('search path contains a non-string: ' + str(elem1))
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        if elem1 != elem2:
            errorReport('Mismatch of orefixes at step ' + str(posn) +
                        ' ' + elem2 + ' expected but ' + elem1 + ' found')
            return False
        posn += 1
    return True       
    
def randomSample() -> list:           # generate a subset of Concept Attributes
    leng = random.randomInt(0, len(_conceptAttributes)-2)
    res = ['name']
    for i in range(leng):
        nextAttr = _conceptAttributes[random.randomInt(0, len(_conceptAttributes)-1)]
        while nextAttr in res:
            nextAttr = _conceptAttributes[random.randomInt(0, len(_conceptAttributes)-1)]
        res.append(nextAttr)
    return res
    
def test_getConceptWithOnlyThese():
    global dkb, _newConceptDict
    
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            userName = 'mpa') 
      
    # work in the mpa Context
    dkb.enter('mpa', 'R')

    restoreRememberedConcepts()    # recover a copy made during newConcept test
    # check whether each one is there and is reachable by all three identifiers

    for nom in _newConceptsDict:    # get each saved tuple
        tup = _newConceptsDict[nom]
        nomSaved = tup[_name]      # the saved Concept name
        assert nom == nomSaved, 'inexplicable inconsitency in saved Concept data'
        for attrib in _conceptAttributes:
            wanted =['name']           # for diagnostics always ask for name
            wanted.append(attrib)      # initially nom there twice, should not matter
            byNom = dkb.getConcept(nom,
                           wanted)     # by simple name; it should be local in Context mpa
            assert getResultAsExpected(byNom, tup, wanted), 'get result for \'' + nom + \
                                                            '\' not as expected' 
            wanted = randomSample(_conceptAttributes)
            byNom = dkb.getConcept(nom,
                           wanted)     # by simple name; it should be local in Context mpa
            assert getResultAsExpected(byNom, tup, wanted), 'get result for \'' + nom + \
                                                            '\' not as expected' 
                                                            
 
def test_getConceptWithOnlyTheseExceptions():
    with pytest.raises(Exceptions.NotAConceptError):
        err1 = dkb.getConcept( 'CAZZZZZZ',
                           onlyThese = ['name'])	  # unknown name
    with pytest.raises(Exceptions.NotAConceptError):
        err1 = dkb.getConcept( 'mpa:CAZZZZZZ',
                           onlyThese = ['name'])	  # unknown name
    with pytest.raises(Exceptions.ContextNotFoundError):
        err1 = dkb.getConcept( 'nemo:CA',
                           onlyThese = ['name'])	  # unknown prefix
    with pytest.raises(Exceptions.NotAConceptError):
        CA2 = dkb.getConcept( 'ex3:mpa:1299:CA',
                           onlyThese = ['name'])	  # PID to a non-existent entry
    with pytest.raises(TypeError):
        CA2 = dkb.getConcept( 'CA',
                           onlyThese = 3)	  # integer instead of list of strings
    with pytest.raises(TypeError):
        CA2 = dkb.getConcept( 'CA',
                           onlyThese = [3])	  # list of integers insted of list of strings
    with pytest.raises(TypeError):
        CA2 = dkb.getConcept( 'CA',
                           onlyThese = [])	  # empty list
    dkb.close()
    with pytest.raises(Exceptions.DKBclosedError):
        CA2 = dkb.getConcept( 'CA',
                           onlyThese = ['name'])	  # cause NameInUse CA exists
 

        

