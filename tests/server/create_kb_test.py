import os

import pytest

from dare_kb.server import DareKB, setting

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    )

KB_ONTO = 'DareKnowledgeBase.owl'

KB = pytest.mark.datafiles(
        os.path.realpath(os.path.join(FIXTURE_DIR, KB_ONTO)),
        )

def kb_onto(datafiles):
    return str(datafiles / KB_ONTO)


@KB
def test_new_darekb_object(tmp_path, datafiles):
    setting.ontology_path = kb_onto(datafiles)
    os.chdir(tmp_path)
    instanceName='ex3KB'
    dkb = DareKB.DareKB(instanceName, base_ontologyfile=setting.ontology_path)
    assert dkb is not None
    return dkb

@KB
def test_new_darekb_initialisation(tmp_path, datafiles):
    dkb = test_new_darekb_object(tmp_path, datafiles)
    dkb._add_default_context()
    # dkb.shutdown()  # Will be done automatically in dkb.__del__
    assert dkb is not None
    return dkb

