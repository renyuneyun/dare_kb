from create_kb_test import (
        KB,
        test_new_darekb_initialisation as initialise_kb,
        )

@KB
def test_new_darekb_default_concept(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    assert dkb.resolve('Concept')
    root_concept = dkb.getConcept('Concept')
    assert root_concept

@KB
def test_darekb_new_context(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    dkb.new_context('ran', 'RandomContext')

@KB
def test_darekb_new_concept(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    dkb.new_context('ran', 'RandomContext')
    dkb.enter('ran', 'W')
    second = dkb.newConcept('SecondConcept')
    dkb.newConcept('ThirdConcept', 'SecondConcept')
    dkb.newConcept('FourthConcept', required={'myprop': 'Number'})
    concept3 = dkb.getConcept('ThirdConcept')
    assert concept3
    assert concept3.label == 'ThirdConcept'
    assert concept3.subClass == dkb.resolve('SecondConcept')[0]

@KB
def test_darekb_new_instance(tmp_path, datafiles):
    dkb = initialise_kb(tmp_path, datafiles)
    dkb.new_context('ran', 'RandomContext')
    dkb.enter('ran', 'W')
    second = dkb.newConcept('SecondConcept')
    inst_pid = dkb.newInst(
            'SecondConcept',
            'SecondConceptInst',
            )
    inst = dkb.getInstance('SecondConceptInst')
    assert inst

