import os

from dare_kb.server import dkb_manage, sbridge, setting

from create_kb_test import (
        KB,
        kb_onto
        )

@KB
def test_newDKB_works(tmp_path, datafiles):
    setting.ontology_path = kb_onto(datafiles)
    os.chdir(tmp_path)
    dkb_manage.new_dkb('ex3KB')

@KB
def test_newDKB_open_works(tmp_path, datafiles):
    test_newDKB_works(tmp_path, datafiles)
    sbridge.open_dkb('ex3KB')
