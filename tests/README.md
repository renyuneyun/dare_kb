Tests
= = = = = = =

This directory contains all the tests for DKB.

It uses (`pytest`)[https://docs.pytest.org/en/latest/] framework. Install it before performing the test.


## How to use

1. Install `pytest`

    e.g., `pip install pytest`

2. Run `run_tests.sh`

    `./run_tests.sh`


## File explained

* `ex2*test.py`: Tests
* `conftest.py`: Configuration of tests, for `pytest` to use
* `run_tests.sh`: A script to easily execute the tests. It exists mainly because the tests have an order.
* `DareKnowledgeBase.owl`: Symbolic link to the *default* world
* `requirements.txt`: The dependency description file

* `ex2gettestonConcepts.py`: Unknown

## TODO

* [ ] Fully migrate `ex2newConcepttest.py` to `pytest`
* [ ] Clean up codes (esp. those commented out) in tests
* [ ] Structure the tests better

