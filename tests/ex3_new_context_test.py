# ex3 for pytest
# Test function new_context
 
import DKBlib as DKB
import pytest
from DKBlib import Exceptions 
import datetime

dkb = None


def eq_list(list1: list,list2: list) -> bool:
    'Return True iff the two arguments are identical lists'
    assert isinstance(list1,list), 'value returned by get_search_path is not a list'
    if not isinstance(list1,list):
       return False
    assert len(list1) == len(list2),\
        'search path not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if len(list1) != len(list2):
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        assert isinstance(elem1, str),\
            'search path contains a non-string: ' + str(elem1)
        if not isinstance(elem1, str):
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        assert elem1 == elem2,\
        	'Mismatch of orefixes at step ' + str(posn) +\
                        ' ' + elem2 + ' expected but ' + elem1 + ' found'
        if elem1 != elem2:
            return False
        posn += 1
    return True
    
    
def test_new_context():
    global dkb
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
                    'mpa',
                    session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now())) 
     
    # check can make a new context
    con1 = dkb.new_context( 'mpa', title = 'ex3 testing' )  # pour moi
                                                  # search path should default to kb
    assert con1 == 'mpa', 'Should return a str that is the prefix'
    
    con2 = dkb.new_context( 'mal', title = 'More ex3 testing', # pour moi aussi
                search_path = ['mpa'] ) # with an explicit search path
    assert con2 == 'mal', "Prefix returned was not \'mal\' which is tres mal"
    
    con3 = dkb.new_context( 'malcolm',             # pour moi encore
                title = 'With explicit owner ex3 testing',
                search_path = ['mpa', 'mal'], # with an explicit search path
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    assert con3 == 'malcolm', "Prefix should be \'malcolm\' it is \'" + con3 + "\'"

def test_new_context_after_enter():   
    dkb.enter('mal', 'W')            
    con4 = dkb.new_context( 'malcolm2',           # pour moi encore
                title = 'With implicit search path ex3 testing',
                owner = 'Malcolm.Atkinson@ed.ac.uk' )
    assert con4 == 'malcolm2', "Prefix should be \'malcolm2\' it is \'" + con4 + "\'"
    
    # check that the search paths are as expected
    # this also checks enter and get_search_path
def test_are_the_search_paths_correct():
    dkb.enter('mpa', 'R')             #test enter again in a different program, assume OK here
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa'
    assert eq_list(sp1, ['kb']), '''mpa's search path not [\'kb\']'''
    
    dkb.enter('mal', 'R')             #check mal's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal'
    assert eq_list(sp2, ['mpa']), '''mal's search path not [\'mpa\']'''
    
    dkb.enter('malcolm', 'R')         #check malcolm's search path
    sp3 = dkb.get_search_path()		  # get search_path for Context 'malcolm'
    assert eq_list(sp3, ['mpa', 'mal']), '''malcolm's search path not ['mpa', 'mal']'''
    
    dkb.enter('malcolm2', 'R')        # check malcolm2's search path
    sp4 = dkb.get_search_path()		  # get search_path for Context 'malcolm2'
    assert eq_list(sp4, ['mal']), '''malcolm2\'s search path not [\'mal\']'''
                                      # Finished checking search paths'
   
def test_new_contextExceptions():
    # check for anticipated exceptions
    #ContextNotFoundError exception is raised when the system cannot find a Context 
    # referenced in search_path.
    with pytest.raises(Exceptions.ContextNotFoundError):
        con5 = dkb.new_context( 'faulty1', title = 'Should not work',
                    search_path = ['nemo'] )	# cause ContextNotFoundError
   
    with pytest.raises(Exceptions.ContextNotFoundError):
       con6 = dkb.new_context('faulty2', title = 'Should not work cyclically', 
                    search_path = ['faulty2'] )	# cause ContextNotFoundError

    # IdentifierWrongError exception is raised when prefix or title is an empty string 
    # or contains inappropriate characters.
    # check 4 provocations
    with pytest.raises(Exceptions.IdentifierWrongError):   
        con7 = dkb.new_context('', "empty string test should fail" )	# empty string not a prefix
                       # IdentifierWrongError exception not raised for empty title

    with pytest.raises(Exceptions.IdentifierWrongError):
        con8 = dkb.new_context('*#€@&^' )	# disallowed characters in prefix
        # IdentifierWrongError exception not raised for title with disallowed characters
        
    dkb.close()	
    #DKBclosedError exception is raised if it is called after dkb.close() has been applied
    
    
    with pytest.raises(Exceptions.DKBclosedError):           
        con11 = dkb.new_context('faulty7' )	# cause DKBclosedError

