# Ex3 pytest file 
# Test function newConcept - simple examples in one Context (mpa)
 
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import logging
import os
import datetime as dt
import sys 
import pickle

_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept

# The attributes every instance in the DKB has automatically
_builtInAttributes = ['name', 'prefix', 'pid', 'timestamp', 'instanceOf', 'state',
                      'predecessor', 'successor', 'mutability', 'extra']

dkb = None						# global shared DKB instance variable set at start
Fish = None						# test identity from newConcept used in getConcept
Eels = None
mtDic = dict()
_the_context = 'newtest'        # the name of the Context in which Concepts are created

    # save a record of Concepts created in a form that is easily pickled and restored
_newConceptsDict = dict()		# a dictionary mapping names to tuples
def rememberConcept(
  nom,							# string its user-supplied name
  superPID,					    # the PID of the Concept it specialises
  reqDic,						# a dictionary of the required name: Concept
  recDic,						# a dictionary of the recommended attributes
  optionalDic, 					# a dictionary of the optional attributes
  translation,                  # a dictionary of attribute name translations
  pid,							# the identifier returned by newConcept
  descr = '',                   # the description if there is one
  ):
    global _newConceptsLDict   	# accumulate 8-tuples there
    tup = (nom, superPID, reqDic, recDic, optionalDic, translation, pid, descr)
    _newConceptsDict.update({nom: tup}) 
    
_name = 0		# index for name
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by newConcept
_descr = 7		# the description of the Concept

_verbose = True				# make True for debug diagnostics
def vprintRememberedConcepts(): #if we're printing list the concepts we have made
    if not _verbose:
        return
    # here chatty
    _sep = '||\t'		# separate parts of a Concept
    print('Created ' + str(len(_newConceptsDict)) + ' new concepts in ' + _the_context )
    print('Parts: \nname' + _sep + 'superConcept' + _sep + 'required' + _sep + 
           'recommended' + _sep + 'optional' + _sep + 'translation' + 
           _sep + 'pid' + _sep + 'description')
    for key in _newConceptsDict:
        tup = _newConceptsDict[key]
        print( 'concept ' + tup[_name] + ':\n' + _sep + 
                tup[_super] + _sep +
                str(tup[_reqs]) + _sep +
                str(tup[_recs]) + _sep + 
                str(tup[_opts]) + _sep + 
                str(tup[_trans]) + _sep + 
                str(tup[_pid]) + _sep +
                str(tup[_descr]))
    print('===\t\tEnd of Concept List\t\t===')

_pickling_dir = '../../pickles/'     # place where pickle files are stored
_file_name = 'mpasConcepts.pkl'    
def saveRememberedConcepts(): # save a pickle copy of all Concepts remembered to test
                              # whether the DKB remembered them in later runs
    if not os.path.isdir(_pickling_dir):
        print( 'Directory for pickling ' + _pickling_dir + ' not found; it will be created')
        os.mkdir(_pickling_dir)
    pickle_path = _pickling_dir + _file_name
    if os.path.isfile(pickle_path):
        print( 'overwritting previous pickle in ' + pickle_path )
        os.remove(pickle_path)
    with open(pickle_path, 'wb') as output:
        pickle.dump( (_the_context, _newConceptsDict), output, pickle.HIGHEST_PROTOCOL )
        output.close()

_context_try = 0
def set_context():
    global _the_context, _context_try
    while True:
        try: 
            dkb.enter(_the_context, 'R')
            _the_context = 'newtest' + str(_context_try)  # if enter succeeds already used
            _context_try += 1
            continue
        except Exceptions.ContextNotFoundError:  # we are trying to get here!
            return              # it has not been used before

def listDic(props):			# print the supplied dictionary 
    for k in props:
        print(k + ': ' + str(props[k]))

def test_newConcept():
    global dkb, Fish, Eels, _verbose
#    if "--verbose" in metafunc.fixturenames:
#        _verbose = True 			# switch on print to std out
    
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            userName = 'mpa')     
    # work in _the_context
    set_context()
    con = dkb.newContext('To test dkb newConcept try ' + str(_context_try), _the_context)
    assert con == _the_context, 'Context for new Concepts incorrect'
    dkb.enter(_the_context, 'W')

    # try simple examples first
    CA = dkb.newConcept('CA') 		#default to top level specialisations of Concept
    CB = dkb.newConcept('CB')       # all 4 dictionaries default to empty
    CC = dkb.newConcept('CC')       # description defaults to the empty string
    # remember those in local list
    rememberConcept('CA', _ConceptPID, mtDic, mtDic, mtDic, mtDic, CA)
    rememberConcept('CB', _ConceptPID, mtDic, mtDic, mtDic, mtDic, CB)
    rememberConcept('CC', _ConceptPID, mtDic, mtDic, mtDic, mtDic, CC)

    # build specialisations to prepare for isa tests, etc.
    CAA = dkb.newConcept('CAA',
        specialisationOf = 'CA') 		# specialisations of CA
    CAB = dkb.newConcept('CAB',
        specialisationOf = 'CA')
    CAC = dkb.newConcept('CAC',
        specialisationOf = 'CA')
    # remember those in local list
    rememberConcept('CAA', CA, mtDic, mtDic, mtDic, mtDic, CAA)
    rememberConcept('CAB', CA, mtDic, mtDic, mtDic, mtDic, CAB)
    rememberConcept('CAC', CA, mtDic, mtDic, mtDic, mtDic, CAC)
    
    CAAA = dkb.newConcept('CAAA', # and again for testing isa inclusion later
        specialisationOf = 'CAA')
    # remember that
    rememberConcept('CAAA', CAA, mtDic, mtDic, mtDic, mtDic, CAAA)
    
    # now explore use of parameter required
    CAAAf = dkb.newConcept('CAAAf', # with one required attribute
        specialisationOf = 'CAA',
        required = {'f':'CA'})	# one property f can be CA or any of its specialisations
    CAAAgh = dkb.newConcept('CAAAgh', # with two required attributes
        specialisationOf = 'CAA',
        required = {'g':'CA',	# one property g can be CA or any of its specialisations
          'h': 'CAC'})		# one property h can be CAC 
    CAAAij = dkb.newConcept('CAAAij', # test recursive structures
        specialisationOf = 'CAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
         'j': 'CAAAij'})		# and another
    # remember those in local list
    rememberConcept('CAAAf', CAA, {'f':CA}, mtDic, mtDic, mtDic, CAAAf)
    rememberConcept('CAAAgh', CAA, {'g': CA, 'h': CAC}, 
                     mtDic, mtDic, mtDic, CAAAgh)
    rememberConcept('CAAAij', CAA, {'i':CAAAij, 'j':CAAAij}, 
                     mtDic, mtDic, mtDic, CAAAij)
   
    # check use of parameter recommended
    CAAAijk = dkb.newConcept('CAAAijk', # add recommendations
        specialisationOf = 'CAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
        'j': 'CAAAij'},			# and another
        recommended = {'k':'CA'}) # and a recommended attribute 
    # remember that one
    rememberConcept('CAAAijk', CAA, {'i':CAAAij, 'j':CAAAij}, 
                     {'k':CA}, mtDic, mtDic, CAAAijk)
   
    # check use of parameter optional
    CAAAijkxxzz = dkb.newConcept('CAAAijkxxzz', # add optional
        specialisationOf = 'CAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
        'j': 'CAAAij'},			    # and another
        recommended = {'k':'CA'},   # and a recommended attribute 
        optional = {'xx': 'String', 'yy': 'Integer'}) #try builtin Concepts for base types
    # remember that one
    rememberConcept('CAAAijkxxzz', CAA, {'i':CAAAij, 'j':CAAAij}, 
                     {'k':CA}, {'xx': 'String', 'yy': 'Integer'}, 
                     mtDic, CAAAijkxxzz)
   
    # check use of parameter translation
    CAAAijkxxzzyy = dkb.newConcept('CAAAijkxxzzyy', # add optional
        specialisationOf = 'CAA',
        required = {'i':'CAAAij',	# one property i references instances of this Concept
        'j': 'CAAAij'},			    # and another
        recommended = {'k':'CA'},   # and a recommended attribute 
        optional = {'xx': 'String', 'yy': 'Integer'}, #try builtin Concepts for base types
        translation = {'name': 'skos:label'} )
    # remember that one
    rememberConcept('CAAAijkxxzzyy', CAA, {'i':CAAAij, 'j':CAAAij}, 
                     {'k':CA}, {'xx': 'String', 'yy': 'Integer'}, 
                     {'name': 'skos:label'}, CAAAijkxxzzyy)

    
    # check optional parameter description
    CAAl = dkb.newConcept(
      'CAAl',
      specialisationOf = 'CAA',
      description = 'testing description as an optional parameter'
    )
    # remember that one
    rememberConcept('CAAl', CAA, mtDic, 
                     mtDic, mtDic, mtDic, CAAl,
                     descr = 'testing description as an optional parameter' )
    # end of testing parameterisations systematically. 
    # NB built-in Concepts not yet available change these later

def test_newConcept_examples():
    global dkb, Fish, Eels
    # try the examples from the documentation to be sure they work
    MarineOrganism = dkb.newConcept(    # a Concept specialising Concept
      'MarineOrganism',     			# to give us something to use and specialise
      description = '''Anything of any kingdom that has lived or does live in any sea on 
      Earth. The plan is to incrementally refine this broad catch all, and to 
      organise observational data under more precise taxonomies.'''
    )
    Fish = dkb.newConcept(    			# one partition of MarineOrganism
      'Fish',
      specialisationOf = 'MarineOrganism',
      description = '''Fish are gill-bearing aquatic craniate animals that lack limbs with   
                              digits.'''
    )
    Teleost = dkb.newConcept(    		# in this specialisation demand more info 
      'Teleost',                		# the bony fish with scales
      description = '''The teleosts or Teleostei (Greek: teleios, "complete" osteon, "bone") 
                    are by far the largest infraclass in the class Actinopterygii, the 
                    ray-finned fishes, and make up 96% of all extant species of fish.''',
      specialisationOf = 'Fish',        # instances will represent Teleost species
      required = {
        'LinnaeanSpeciesName': 'String'}     # binomial species name instance of String
    )
    Chondrichthyes = dkb.newConcept(    	# and ask politely how big for 
      'Chondrichthyes',        				# the cartilaginous fish
      description = '''Chondrichthyes (from Greek chondr- 'cartilage', ichthys 'fish') is a 
                    class that contains the cartilaginous fishes. They are jawed 
                    vertebrates with paired fins, paired nares, scales, a heart with its 
                    chambers in series, and skeletons made of cartilage rather than 
                    bone.''',
      specialisationOf = 'Fish',    		# instances will represent species
      required = {
        'LinnaeanSpeciesName': 'String'},     #insist on a binomial species name
      recommended = {
        'size': 'Number',        # if available, average adult length in metres
        'mass': 'Number'}        # independently, if available, average adult mass in kilos
                           	# later, a time series of these will show species viability
    )
    Eels = dkb.newConcept(
    'Eels',                	# demo of choice of value types
    specialisationOf = 'Teleost',            	# a group of fin and bone species
    description = 'An eel is any ray-finned fish belonging to the order Anguilliformes',
    required = {
        'LinnaeanSpeciesName': 'String'},     #insist on a binomial species name
      recommended = {
        'size': 'Number',        # if available, average adult length in metres
        'mass': 'Number',        # independently, if available, average adult mass in kilos
      #  'range': [ 'Geoname','DepthRange', 'LatRange', 'LongRange' ],    # where found
        }
    )                       # later, a time series of these will show species viability    
    # remember those documentation examples
    rememberConcept('MarineOrganism', _ConceptPID, mtDic, mtDic, mtDic, 
                     mtDic, MarineOrganism,
                     descr = '''Anything of any kingdom that has lived or does live in any sea on 
      Earth. The plan is to incrementally refine this broad catch all, and to 
      organise observational data under more precise taxonomies.''')
    rememberConcept('Fish', MarineOrganism, mtDic, mtDic, mtDic, mtDic, Fish,
                     descr = '''Fish are gill-bearing aquatic craniate animals that lack limbs with   
                              digits.''')
    rememberConcept('Teleost', Fish, {'LinnaeanSpeciesName': 'String'}, mtDic,
                     mtDic,  mtDic, Teleost,
                     descr = '''The teleosts or Teleostei (Greek: teleios, "complete" osteon, "bone") 
                    are by far the largest infraclass in the class Actinopterygii, the 
                    ray-finned fishes, and make up 96% of all extant species of fish.''')
    rememberConcept('Chondrichthyes', Fish, {'LinnaeanSpeciesName': 'String'},
                    {'size': 'Number', 'mass': 'Number'}, mtDic, mtDic, 
                    Chondrichthyes,
                    descr = '''Chondrichthyes (from Greek chondr- 'cartilage', ichthys 'fish') is a 
                    class that contains the cartilaginous fishes. They are jawed 
                    vertebrates with paired fins, paired nares, scales, a heart with its 
                    chambers in series, and skeletons made of cartilage rather than 
                    bone.''')
    rememberConcept('Eels', Teleost, {'LinnaeanSpeciesName': 'String'},
                    {'size': 'Number', 'mass': 'Number', 
                    #  'range': [ 'Geoname','DepthRange', 'LatRange', 'LongRange' ]
                    }, mtDic, mtDic, Eels,
                    descr = 'An eel is any ray-finned fish belonging to the order Anguilliformes' )
    # also check Widget and SpecialWidget work
    Widget = dkb.newConcept('Widget',
                    description = 'An example',
                    required = {'shape': 'String'})
    SpecialWidget = dkb.newConcept('SpecialWidget',
                    description = 'A specialisation of Widget',
                    specialisationOf = Widget,
                    recommended = {'length': 'Number', 'weight': 'Number'})
    rememberConcept('Widget', _ConceptPID, {'shape': 'String'},
                    mtDic, mtDic, mtDic, Widget,
                    descr = 'An example' )
    rememberConcept('SpecialWidget', Widget, {'shape': 'String'},
                    { 'length': 'Number', 'weight': 'Number' }, mtDic, mtDic, SpecialWidget,
                    descr = 'A specialisation of Widget' )                    
    vprintRememberedConcepts()  # what have we tried to do?
    saveRememberedConcepts()    # save a copy for subsequent checking
    # end of testing the examples in our documentation
    # the functionality of newConcept will be tested by ex3get_test.py

def test_newConcept_exceptionThrowing():
    global dkb
    with pytest.raises(Exceptions.NameInUseError):
        CA2 = dkb.newConcept( 'CA')	  # cause NameInUse CA exists
    with pytest.raises(Exceptions.NotAConceptError):
        CFaulty = dkb.newConcept( 'CFaulty',
                         specialisationOf = 'mpa5' )	# Not a Concept
    dkb.close()	#finished this test
    with pytest.raises(Exceptions.DKBclosedError):
        CFaultyAgain = dkb.newConcept( 'CFaultyAgain' )	# cause DKBclosedError
