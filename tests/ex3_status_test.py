# Ex3 
# Test status function
 
import DKBlib as DKB
from DKBlib import Exceptions
import pytest 
import datetime

dkb = None

_number_across = 5
def print_wrapped(id: str, values: list):
    len_list = len(values)
    i = 0
    print(id + ':')
    next_line = '[ '
    while i < len_list:
        on_this_line = 0
        while i < len_list and on_this_line < _number_across:
            next_line = next_line + values[i]
            i += 1
            on_this_line += 1
            if i != len_list:
                next_line = next_line + ',\t'
            else:
                next_line = next_line + ' ]'
        print(next_line)
    
def test_status():
    global dkb
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now())) 
      
    # just get it and print to see what we have
    status_dic = dkb.status()        # get a dictionary covering the status
    assert isinstance(status_dic, dict) # Should be a dictionary      
    print('the returned dictionary has ' +
            str(len(status_dic)) + ' items')
    for id in status_dic:
        if id == 'all_contexts' or id == 'concepts':
            print_wrapped(id, status_dic[id])
        else:
            print(id + ':\t' + str(status_dic[id]))
    dkb.close()	#finished this test
    
def test_statusExceptions():
    #DKBclosedError exception is raised if it is called after dkb.close() has been applied.
    with pytest.raises(Exceptions.DKBclosedError):
         stat = dkb.status()		# shouldn't work after it has been closed

