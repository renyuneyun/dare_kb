# Ex3 pytsest test functions 
# Test rate of working
 
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
from time import process_time
from typing import List
import datetime

dkb = None
_targetRate = 0.1           # time to perform one cycle of Context operations in seconds
_reps = 10                  # number of repeats


def eq_list(list1: List[str], list2: List[str]) -> bool:
    'Return True iff the two arguments are identical lists'
    assert isinstance(list1,list), 'value returned by get_search_path is not a list'
    if not isinstance(list1,list):
       return False
    assert len(list1) == len(list2),\
        'search path not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if len(list1) != len(list2):
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        assert isinstance(elem1, str),\
            'search path contains a non-string: ' + str(elem1)
        if not isinstance(elem1, str):
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        assert elem1 == elem2,\
        	'Mismatch of orefixes at step ' + str(posn) +\
                        ' ' + elem2 + ' expected but ' + elem1 + ' found'
        if elem1 != elem2:
            return False
        posn += 1
    return True
    
def test_repeats():
    'login, start timer, then repeat _reps times a cycle of Context functions'
    global dkb, _reps
#    parser = argparse.ArgumentParser()
#    parser.add_argument("repetitions", help = "Specify number of action-pattern repeats",
#                        type=int)
#    args = parser.parse_args()
#    _reps = args.repetitions
    tb4Login = process_time()
    dkb = DKB.login('ex3KB', username = 'mpa',
                     session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now()))
    dkb.enter('mpa', 'R')
    startTime = process_time()
    assert startTime - tb4Login <= 0.1, "start time too slow"		# ensure fast start up
    expectedContexts = []
    previousContext = 'mpa'
    # build the right sized testing ground
    for i in range(_reps):
        dkb.enter(previousContext, 'W')
        newPrefix = 'test' + str(i)
        expectedContexts.append(newPrefix)
        con = dkb.new_context(newPrefix,
                             title = 'Stress testing context functions ' + str(i))
        assert con == newPrefix, "new_context failed to return " + newPrefix
        dkb.enter(con, 'W')
        sp = dkb.get_search_path()
        assert eq_list(sp, [previousContext])
        previousContext = con
        sp.extend(['mal','kb'])
        dkb.set_search_path(sp)
        dkb.leave()
    # simulate mix 4 parts read to 1 part write at this scale
    for i in range(2):			# two lots of reads
        for p in expectedContexts:
            dkb.enter(p, 'R')
            sp2 = dkb.get_search_path()
    for p in expectedContexts:
        dkb.enter(p, 'W')
        sp3 = dkb.get_search_path()
        sp3.extend(['mpa2', 'mal4'])
        dkb.set_search_path(sp3)
    for i in range(2):			# two lots of reads
        for p in expectedContexts:
            dkb.enter(p, 'R')
            sp2 = dkb.get_search_path()
    endTime = process_time()
    elapsedTime = endTime - startTime
    assert elapsedTime <= _reps*_targetRate, "Unit time too slow"		# ensure fast rate of working
    dkb.close()
    assert process_time() - endTime <= 0.1, "shutdown too slow"		# ensure fast shutdown
    
