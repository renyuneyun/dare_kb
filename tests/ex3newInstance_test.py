# Ex3 pytest FileCache
# Testing the production of new instances using newInst

import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import pickle
import random
import os

_attributes_working = True
_state_settable = True
_extra_working = False             # unspecified properties are not permitted in instances
_verbose = True
_indent = 4*' '
_separator = ':'

_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept

dkb = None                         # set on login to DKB

    # save a record of instances created in a form that is easily pickled and restored
_newInstancesDict = dict()		# a dictionary mapping names to tuples
def rememberInstance(
  nom: str,						# its user-supplied name
  instanceOfPID: str,		    # the PID of the Concept it specialises
  attributesDic: dict,			# a dictionary of the supplied attribute identifier value pairs
  instPID: str                  # the PID of the new instance
  ):
    global _newInstancesLDict   	# accumulate 4-tuples there
    tup = (nom, instanceOfPID, attributesDic, instPID)
    _newInstancesDict.update({nom: tup}) 
    
_name = 0		# index for name
_instOf = 1		# the Concept this is an instance of
_attribs = 2	# the supplied properties
_instPID = 3    # the resulting PID

newConceptsDict = dict()           # recovered saved copy of the Concepts created
_pickling_dir = '../../pickles/'   # place where pickle files are stored
_file_name = 'mpasConcepts.pkl'    # file where Concepts created were also pickled for tests like this
_concept_prefix = ''               # Context where Concepts were last created
_instance_prefix = ''
_alreadyRestored = False
def restoreRememberedConcepts(): # recover from the file made by saveRememberedConcepts
    global _newConceptsDict, _alreadyRestored, _concept_prefix, _instance_prefix
    if _alreadyRestored:
        return
    _alreadyRestored = True
    if not os.path.isdir(_pickling_dir):
        print( 'Directory for pickling ' + _pickling_dir + ' not found; Concepts not recovered')
        exit
    pickle_path = _pickling_dir + _file_name
    if not os.path.isfile(pickle_path):
        print( 'Pickled Concept file ' + pickle_path + ' not found; Concepts not recovered.' +
         'Run \npytest ex3newConcept_test.py\nto create the Concepts' )
        exit
    with open(pickle_path, 'rb') as input:
        _concept_prefix, _newConceptsDict = pickle.load(input)
        _instance_prefix = _concept_prefix         # first time, instances and Concepts co-located
# Tuple offsets to extract from remembered Concepts
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by newConcept
_descr = 7		# the description of the Concept

_verbose = False				# make True for debug diagnostics
mtDic = dict()
reps = 3						# population target size set by --reps
_instance_file_name = 'mpasInstances.pkl'

def saveRememberedInstances(): # save a pickle copy of all instances remembered to test
                              # whether the DKB remembered them in later runs, i.e., testing get
    if not os.path.isdir(_pickling_dir):
        print( 'Directory for pickling ' + _pickling_dir + ' not found; it will be created')
        os.mkdir(_pickling_dir)
    pickle_path = _pickling_dir + _instance_file_name
    if os.path.isfile(pickle_path):
        print( 'overwritting previous pickle in ' + pickle_path )
        os.remove(pickle_path)
    with open(pickle_path, 'wb') as output:
        pickle.dump( (_instance_prefix, _newInstancesDict), output, pickle.HIGHEST_PROTOCOL )
        output.close()

_states = ['new', 'updated', 'valid', 'authoritative', 'depricated', 'discarded']        
def choose_state() -> str:     # chooses a possible state
    return _states[random.randint(0, len(_states) - 1)]

_mutabilities = ['mutable', 'no hjiding', 'frozen']
def choose_mutability() -> str:
    return _mutabilities[random.randint(0, len(_mutabilities) - 1)]

def get_name_from_pid(pid: str) -> str:
    _num_seps = pid.count(_separator)
    assert _num_seps == 3, "malformed pid"
    pid_parts = pid.split( sep = _separator, maxsplit = 4)
    return pid_parts[3]
    
def print_instance_creation(
    inst_name: str, 
    Concept_pid: str, 
    new_inst_pid: str,
    properties: dict,
    more_properties: dict     # not included in properties and stored instance if NOT _attribute_working
    ):
    concept_name = get_name_from_pid(Concept_pid)
    print( inst_name + ' = ' +concept_name + '(' )
    for prop in properties:
        print( _indent + prop + ' = ' + str(properties[prop]) )
    if not _attributes_working:
        print('omitted')
        for prop in more_properties:
            print( _indent + prop + ' = '  +str(more_properties[prop]) )
    print( _indent + ')' )

_context_try = 0
def change_context():
    global _context_try, _instance_prefix  # reset these if find already used current context
    _instance_prefix = _concept_prefix + 'instances' + str(_context_try)
    while True:
        try: 
            dkb.enter(_instance_prefix, 'R')
            _context_try += 1
            _instance_prefix = _concept_prefix + 'instances' + str(_context_try)  # if enter succeeds already used
            continue
        except Exceptions.ContextNotFoundError:  # we are trying to get here! prefix available
            con = dkb.newContext('To test dkb newInst try ' + str(_context_try), _instance_prefix)
            assert con == _instance_prefix, 'prefix produced does not match prefix requested'
            dkb.enter(_instance_prefix, 'W')     # we will create instances there
            dkb.setSearchpath([_concept_prefix]) # arrange name lookup of Concepts works in new Context
            return                               # switched to Context not used before

def populateConcept(conName: str,
                    moreProps = mtDic):
    Concept_pid = _newConceptsDict[conName][_pid]
    while 'a' + conName + '0' in _newInstancesDict:
        conName = 'z' + conName    # avoid re-using same name
    for i in range(reps):
        instName = 'a' + conName + str(i)
        properties = {'name': instName}
        if _attributes_working:
            properties.update(moreProps)
        # add some variation testable without attributes in get and find
        if _state_settable and random.randint(0,10) > 8:
            universal_properties = {'state': choose_state(), 
                                    'mutability': choose_mutability()}
            properties.update(universal_properties)
        # print('name = ' + instName + ' Concept = ' + conName + ' with PID ' + Concept_pid)
        while True:
            try:
                newInstPID = dkb.newInst(Concept_pid, **properties)
                break                          # it worked Context not in use
            except Exceptions.NameInUseError:  # here if the Context used already
                change_context()
        rememberInstance(instName, Concept_pid, properties, newInstPID)  
        if _verbose:
            print_instance_creation(instName, Concept_pid, newInstPID, properties, moreProps)  
        
def test_newInst():
            # test making instances of each of the 'artificial' Concepts produced in
            # the newConcept tests. The code structure below mirrors that structure
            # and the pickled Concepts are read to recover PIDs, etc.
    global dkb, reps    # set dkb on login for other tests and reps from CL eventually
#    if "--verbose" in metafunc.fixturenames:
#        _verbose = True 			# switch on print to std out
    restoreRememberedConcepts()    # recover a copy made during newConcept test in the recovered Context
    _instance_prefix = _concept_prefix    # try using the same Context unless it has been used before
    # login to a DKB in its current state and start to use it
    dkb = DKB.login('ex3KB',
            userName = 'mpa')     
    # work in the last saved Context
    dkb.enter(_instance_prefix, 'W')  # changed by populateConcept if already used

    # try simple examples first
    #CA = dkb.newConcept('CA') 		#default to top level specialisations of Concept
    populateConcept('CA')
    #CB = dkb.newConcept('CB')       # all 4 dictionaries default to empty
    populateConcept('CB')
    #ßCC = dkb.newConcept('CC')       # description defaults to the empty string
    populateConcept('CC')
    # build specialisations to prepare for isa tests, etc.
    # CAA = dkb.newConcept('CAA',
    #     specialisationOf = 'CA') 		# specialisations of CA
    populateConcept('CAA')
    # CAB = dkb.newConcept('CAB',
    #    specialisationOf = 'CA')
    populateConcept('CAB')
    # CAC = dkb.newConcept('CAC',
    #     specialisationOf = 'CA')
    populateConcept('CAC')    
    # CAAA = dkb.newConcept('CAAA', # and again for testing isa inclusion later
    #     specialisationOf = 'CAA')
    populateConcept('CAAA')   
    # now explore use of parameter required
    # CAAAf = dkb.newConcept('CAAAf', # with one required attribute
    #     specialisationOf = 'CAA',
    #     required = {'f':'CA'})	# one property f can be CA or any of its specialisations
    populateConcept('CAAAf',
        moreProps = {'f': _newInstancesDict['aCA0'][_instPID]})
    populateConcept('CAAAf',
        moreProps = {'f': _newInstancesDict['aCAA0'][_instPID]})
    populateConcept('CAAAf',
        moreProps = {'f': _newInstancesDict['aCAB0'][_instPID]})
    populateConcept('CAAAf',
        moreProps = {'f': _newInstancesDict['aCAC0'][_instPID]})
    populateConcept('CAAAf',
        moreProps = {'f': _newInstancesDict['aCAAA0'][_instPID]})    
    # CAAAgh = dkb.newConcept('CAAAgh', # with two required attributes
    #     specialisationOf = 'CAA',
    #     required = {'g':'CA',	# one property g can be CA or any of its specialisations
    #                 'h': 'CAC'})		# one property h can be CAC 
    populateConcept('CAAAgh',
        moreProps = {'g': _newInstancesDict['aCA0'][_instPID],
                    'h': _newInstancesDict['aCAC0'][_instPID]})                    
    # CAAAij = dkb.newConcept('CAAAij', # test recursive structures
    #     specialisationOf = 'CAA',
    #     required = {'i':'CAAAij',	# one property i references instances of this Concept
    #     'j': 'CAAAij'})		# and another
    populateConcept('CAAAij',
        moreProps = {'i': None,            # or some other way to indicate null update later
                    'j': None})
    # check use of parameter recommended
    # CAAAijk = dkb.newConcept('CAAAijk', # add recommendations
    #     specialisationOf = 'CAA',
    #     required = {'i':'CAAAij',	# one property i references instances of this Concept
    #                 'j': 'CAAAij'},			# and another
    #     recommended = {'k':'CA'}) # and a recommended attribute 
    populateConcept('CAAAijk',
        moreProps = {'i': _newInstancesDict['aCAAAij0'][_instPID],
                    'j': _newInstancesDict['aCAAAij1'][_instPID],
                    'k': _newInstancesDict['aCAA0'][_instPID]})  
    # check use of parameter optional
    # CAAAijkxxzz = dkb.newConcept('CAAAijkxxzz', # add optional
    #     specialisationOf = 'CAA',
    #     required = {'i':'CAAAij',	# one property i references instances of this Concept
    #     'j': 'CAAAij'},			    # and another
    #    recommended = {'k':'CA'},   # and a recommended attribute 
    #    optional = {'xx': 'String', 'yy': 'Integer'}) #try builtin Concepts for base types
    populateConcept('CAAAijkxxzz',
        moreProps = {'i': _newInstancesDict['aCAAAij0'][_instPID],
                    'j': _newInstancesDict['aCAAAij1'][_instPID],
                    'xx': "abcd",
                    'yy': -23})  
    # check use of parameter translation
    # CAAAijkxxzzyy = dkb.newConcept('CAAAijkxxzzyy', # add optional
    #     specialisationOf = 'CAA',
    #    required = {'i':'CAAAij',	# one property i references instances of this Concept
    #    'j': 'CAAAij'},			    # and another
    #    recommended = {'k':'CA'},   # and a recommended attribute 
    #    optional = {'xx': 'String', 'yy': 'Integer'}, #try builtin Concepts for base types
    #    translation = {'name': 'skos:label'} )
    populateConcept('CAAAijkxxzzyy',
        moreProps = {'i': _newInstancesDict['aCAAAij0'][_instPID],
                    'j': _newInstancesDict['aCAAAij0'][_instPID],
                    'xx': "abcdefg",
                    'yy': -29})   
    # check optional parameter description
    # CAAl = dkb.newConcept(
    #   'CAAl',
    #  specialisationOf = 'CAA',
    #  description = 'testing description as an optional parameter'
    # )
    populateConcept('CAAl')
    

def test_newInstOnConcept_examples():
    # test making instances of the Concept examples with one instance per example
    # code below reflects pattern in the newConcept tests test_newConcept_examples    
    global reps          # set reps here to limit population growth to 1 per Concept 
    reps = 1
    # try the examples from the documentation to be sure they work
    # MarineOrganism = dkb.newConcept(    # a Concept specialising Concept
    #   'MarineOrganism',     			# to give us something to use and specialise
    #   description = '''Anything of any kingdom that has lived or does live in any sea on 
    #   Earth. The plan is to incrementally refine this broad catch all, and to 
    #  organise observational data under more precise taxonomies.'''
    #)
    populateConcept('MarineOrganism')
    # Fish = dkb.newConcept(    			# one partition of MarineOrganism
    #   'Fish',
    #   specialisationOf = 'MarineOrganism',
    #   description = '''Fish are gill-bearing aquatic craniate animals that lack limbs with   
    #                          digits.'''
    #)
    populateConcept('Fish')
    # Teleost = dkb.newConcept(    		# in this specialisation demand more info 
    #  'Teleost',                		# the bony fish with scales
    #   description = '''The teleosts or Teleostei (Greek: teleios, "complete" osteon, "bone") 
    #                 are by far the largest infraclass in the class Actinopterygii, the 
    #                 ray-finned fishes, and make up 96% of all extant species of fish.''',
    #   specialisationOf = 'Fish',        # instances will represent Teleost species
    #   required = {
    #     'LinnaeanSpeciesName': 'String'}     # binomial species name instance of String
    # )
    populateConcept('Teleost',
                    moreProps = {'LinnaeanSpeciesName': 'Gadus morhua',
                                 'popular_name': 'cod'})
    # Chondrichthyes = dkb.newConcept(    	# and ask politely how big for 
    #   'Chondrichthyes',        				# the cartilaginous fish
    #   description = '''Chondrichthyes (from Greek chondr- 'cartilage', ichthys 'fish') is a 
    #                 class that contains the cartilaginous fishes. They are jawed 
    #                 vertebrates with paired fins, paired nares, scales, a heart with its 
    #                 chambers in series, and skeletons made of cartilage rather than 
    #                 bone.''',
    #   specialisationOf = 'Fish',    		# instances will represent species
    #   required = {
    #    'LinnaeanSpeciesName': 'String'},     #insist on a binomial species name
    #   recommended = {
    #     'size': 'Number',        # if available, average adult length in metres
    #     'mass': 'Number'}        # independently, if available, average adult mass in kilos
    #                        	# later, a time series of these will show species viability
    # )
    populateConcept('Chondrichthyes',
                    moreProps = {'LinnaeanSpeciesName': 'Manta birostris',
                                 'popular_name': 'giant manta',
                                 'size': 7, #metres
                                  'mass': 1350})  #kilograms
    # Eels = dkb.newConcept(
    # 'Eels',                	# demo of choice of value types
    # specialisationOf = 'Teleost',            	# a group of fin and bone species
    # description = 'An eel is any ray-finned fish belonging to the order Anguilliformes',
    # required = {
    #    'LinnaeanSpeciesName': 'String'},     #insist on a binomial species name
    #  recommended = {
    #   'size': 'Number',        # if available, average adult length in metres
    #    'mass': 'Number',        # independently, if available, average adult mass in kilos
      #  'range': [ 'Geoname','DepthRange', 'LatRange', 'LongRange' ],    # where found
    #    }
    # )                       # later, a time series of these will show species viability
    populateConcept('Eels',
                    moreProps = {'LinnaeanSpeciesName': 'Ammodytes tobianus',
                                 'popular_name': 'lesser sand eel',
                                 'size': 0.02})      # metres
    # also check Widget and SpecialWidget work
    # Widget = dkb.newConcept('Widget',
    #                 description = 'An example',
    #                 required = {'shape': 'String'})
    populateConcept('Widget',
                    moreProps = {'shape': 'cuboid'})
    # SpecialWidget = dkb.newConcept('SpecialWidget',
    #                 description = 'A specialisation of Widget',
    #                 specialisationOf = Widget,
    #                recommended = {'length': 'Number', 'weight': 'Number'})
    populateConcept('SpecialWidget',
                    moreProps = {'shape': 'elipsoid',
                                 'length': 15,
                                  'weight': 3.75})
    saveRememberedInstances()    # save a copy for subsequent checking    
    
    
def test_newInst_exceptionThrowing():
    with pytest.raises(Exceptions.NameInUseError):
        wrongCAinstance = dkb.newInst( 'CA',
                           **{'name': 'aCA0'})	  # cause NameInUse aCA0 exists
    with pytest.raises(Exceptions.NotAConceptError):
        wrongAgain = dkb.newInst( 'aCA0',            # can't make an instance of an instance
                         **{'name': 'wrongAgain'} )	# Not a Concept
    dkb.close()	#finished this test
    with pytest.raises(Exceptions.DKBclosedError):
        tooLateAgain = dkb.newInst( 'CA', **{'name': 'tooLateAgain'} )	# cause DKBclosedError