import pathlib
import sys


### Add DKBlib into the search path
### This may not be necessary if we split DKBlib and dkb_server into separate packages

# sys.path does not support `Path`s yet
this_dir = pathlib.Path(__file__).resolve()
sys.path.insert(0, str(this_dir.parents[1] / 'dare_kb'))

def pytest_addoption(parser):
    parser.addoption("--vv", action="store_true", help="diagnostic prints to standard out switched on")
    parser.addoption("--reps", action="store", type = int, default = 10, 
                      help = 'number of times to repeat the test load' )
                      
