# test context_status

import DKBlib as DKB
from DKBlib import Exceptions
import pytest 
import datetime
from typing import Any, Dict, List

dkb = None
_verbose = True

_number_across = 5
def print_wrapped(id: str, values: list):
    len_list = len(values)
    i = 0
    print(id + ':')
    next_line = '[ '
    while i < len_list:
        on_this_line = 0
        while i < len_list and on_this_line < _number_across:
            next_line = next_line + values[i]
            i += 1
            on_this_line += 1
            if i != len_list:
                next_line = next_line + ',\t'
            else:
                next_line = next_line + ' ]'
        print(next_line)

def vprint( s: str):
    if _verbose:
        print(s)

_line_width = 100
_sep = ', '
_sep_len = len(_sep)
def vprint_mc( hdr: str, body):
    if not _verbose:
        return
    used = len(hdr) + 3
    line = hdr + ' '
    first = True
    if type(body) == list:
        line += '[ '
        for nxt in body:
            if first:
                nxt_item = str(nxt)
                first = False
            else:
                nxt_item = _sep + str(nxt)
            uses = len(nxt_item)
            if used + uses > _line_width:
                print(line)
                used = uses
                line = nxt_item
            else:
                line += nxt_item
                used += uses
        line += ' ]'
        print(line)
    elif type(body) == dict:
        line += '{ '
        for nxt in body:
            val = str(body[nxt])
            nxt_item = str(nxt) + ': ' + val
            if first:
                first = False
            else:
                nxt_item = _sep + nxt_item
            uses = len(nxt_item)
            if used + uses > _line_width:
                print(line)
                used = uses
                line = nxt_item
            else:
                line += nxt_item
                used += uses
        line += ' }'
        print(line)
    else:
        print(line + ' body is ' + str(type(body)))
        print(str(body))

def print_context_status(stat: Dict[str, Any]):
    print('\nStatus for ' + stat["prefix"])
    print('identifier: ' + stat["identifier"])
    print('owner: ' + stat["owner"])
    print('search_path: ' + str(stat["search_path"]))
    print('state: ' + stat["state"])
    print('title: ' + stat["title"])
    print('mode: ' + str(stat["mode"]))
    vprint_mc('concepts:', stat["concepts"])
    vprint_mc('instances:', stat["instances"])
    

def valid_context_status( res: Dict[str, Any],
                          explain: str,
                          expected: Dict[str, Any]) -> bool:
    assert type(res) == dict, "returned result not a dictionary for " + explain
    if _verbose:
        print_context_status(res)
    for k in expected:
        assert k in res,  "Expected entry for \'" + k + "\' not in result for " + explain
        if expected[k] != None:     # use None to signal don't check
            assert res[k] == expected[k], "Value for " + k + " not that expected for " + explain
    return True

def try_context(prefix: str, expected: Dict[str, Any]):
    res = dkb.context_status(prefix)
    assert valid_context_status( res,
                         'an explicit enquiry about the state of ' + prefix,
                         expected), 'problems with the context_status for ' + prefix
    
def test_context_status_no_contents():
    global dkb
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now()))
    dkb.enter('mpa', 'R')
    dic_kb = dkb.context_status()      
    assert valid_context_status(dic_kb,
                        'status of current context which is mpa',
                        {'identifier': None,
                        'owner': None,
                        'prefix': 'mpa',
                        'search_path': ["kb"],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': 'R'}), "problems with default as current context"
    try_context('mpa2', 
                {'identifier': None,
                        'owner': None,
                        'prefix': 'mpa2',
                        'search_path': ['mal7'],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': None})
    dkb.enter('mpa2', 'R')
    try_context('mpa2',             # test mode setting
                {'identifier': None,
                        'owner': None,
                        'prefix': 'mpa2',
                        'search_path': ['mal7'],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': 'R'})
    dkb.enter('mpa2', 'W')
    try_context('mpa2',             # test mode setting
                {'identifier': None,
                        'owner': None,
                        'prefix': 'mpa2',
                        'search_path': ['mal7'],
                        'title': None,
                        'concepts': {},
                        'instances': {},
                        'mode': 'W'})
                        
def test_context_status_with_contents():  # may do this in a separate file
    pass                      # write after new_concept and new_inst testing

def test_context_status_exceptions():
    # non-existing context
    with pytest.raises(Exceptions.ContextNotFoundError):
        dkb.context_status( 'nemo' )	  # cause ContextNotFoundError
    #DKBclosedError exception is raised if it is called after dkb.close() has been applied.
    dkb.close()
    with pytest.raises(Exceptions.DKBclosedError):
         stat = dkb.context_status()		# shouldn't work after it has been closed