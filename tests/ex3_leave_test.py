# Ex3 pytest tests 
# Test function leave
 
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import datetime

dkb = None


def eq_list(list1: str, list2: str) -> bool:
    'Return True iff the two arguments are identical lists'
    assert isinstance(list1,list), 'value returned by get_search_path is not a list'
    if not isinstance(list1,list):
       return False
    assert len(list1) == len(list2),\
        'search path not the length expected: returned = ' +\
                    str(len(list1)) + ' but expected = ' +\
                    str(len(list2))
    if len(list1) != len(list2):
        return False
    posn = 0			# how far down the search path are we?
    for elem1 in list1:
        assert isinstance(elem1, str),\
            'search path contains a non-string: ' + str(elem1)
        if not isinstance(elem1, str):
            return False
        elem2 = list2.pop(0)   # get and remove the first expected prefix
        assert elem1 == elem2,\
        	'Mismatch of orefixes at step ' + str(posn) +\
                        ' ' + elem2 + ' expected but ' + elem1 + ' found'
        if elem1 != elem2:
            return False
        posn += 1
    return True
    
    
def test_leave():
    global dkb
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            'mpa',
            session_id = 'Malcolm.Atkinson@ed.ac.uk' +str(datetime.datetime.now())) 
    # Make some contexts to do the test with
    dkb.enter('mpa', 'R')
    con1 = dkb.new_context( 'mpa7', title = 'ex3 testing leave')      # pour moi
    dkb.enter('mpa7', 'R')            # enter new Context to test its search path
    sp1 = dkb.get_search_path()		  # get search_path for Context 'mpa7'
    assert eq_list(sp1, ['mpa']), '''mpa7\'s search path not [\'mpa\']'''
    
    # now test leave
    dkb.leave()
    con2 = dkb.new_context( 'mal7', title = 'More ex3 testing of leave' ) # pour moi aussi
    dkb.enter('mal7', 'R')            # inspect mal7's search path
    sp2 = dkb.get_search_path()		  # get search_path for Context 'mal7'
    assert eq_list(sp2, ['kb']), '''mal7\'s search path not [\'kb\']'''
   
def test_leaveExceptions():
    dkb.close()	#finished this test
    with pytest.raises(Exceptions.DKBclosedError):
        dkb.leave()	                   # cause DKBclosedError
     