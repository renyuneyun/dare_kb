# Pytest to test whether dkb.find is working on Concepts and instances produced by new* tests
import DKBlib as DKB
import pytest
from DKBlib import Exceptions
import pickle
import os

_verbose = True
def printv(s: str):
    if _verbose:
        print(s)

_indent = 4*' '
_separator = ':'

mt_dic = {}     # many occasions an empty dictionary is needed

                # offsets to unpack a remembered Concept 
_name = 0		# index for name
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by newConcept
_descr = 7		# the description of the Concept

_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept in ex3KB installation
dkb = None		

def vprint( s: str):
    if _verbose:
        print(s)
        
def eqDic(dic1: dict, dic2:dict,     # true iff the two supplied dictionaries are equal
          name:str) -> bool:
    assert len(dic1) == len(dic2), 'The two dictionaries, \'' + str(dic1) + '\' and \'' + \
                      str(dic2) + '\' have different lengths for ' + name 
    if len(dic1) != len(dic2):
        return False
    for key in dic1:
        assert key in dic2, '\'' + key + '\' in ' + str(dic1) + \
                        ' but not in ' + str(dic2)
        if not key in dic2:
            return False
        assert dic1[key] == dic2[key], 'The values for \'' + key + '\' differ' 
        if dic1[key] != dic2[key]:
            return False
    return True
  
def set_controls():             # override any of the default controls from the command line
    global _verbose
    pass                        # to be implemented  

    # recover a record of Concepts created from pickled file
_newConceptsDict = dict()		# a dictionary mapping names to tuples
_alreadyRestored = False
_pickling_dir = '../../pickles/'     # place where pickle files are stored
_concept_prefix = ''

def restore_pickled_Concepts():
    global _newConceptsDict, _alreadyRestored, _concept_prefix
    if _alreadyRestored:
        return
    _alreadyRestored = True
    if not os.path.isdir(_pickling_dir):
        print( 'Directory for pickling ' + _pickling_dir + ' not found; Concepts not recovered.' + 
                'Running\npytest ex3newConcept_test.py\nshould fix it.' )
        exit
    pickle_path = _pickling_dir + 'mpasConcepts.pkl'
    if not os.path.isfile(pickle_path):
        print( 'Pickled Concept file ' + pickle_path + ' not found; Concepts not recovered.' + 
                'Running\npytest ex3newConcept_test.py\nshould fix it.' )
        exit
    with open(pickle_path, 'rb') as input:
        _concept_prefix, _newConceptsDict = pickle.load(input)

def isaConcept(res):            # given a dictionary returned by a get test if it is
                                # a specialisation directly or indirectly of Concept
    return res['instanceOf'] == _ConceptPID
    
def isaSpecialisationOf(res,    # test whether the dictionary represents a 
                        aConcept): # directly or indirectly of the supplied Concept
    assert res.has_key('specialisationOf'), 'No specialisationOf attribute present => not a well-formed Concept'
    if not res.has_key('specialisationOf'):
        return False
    itSpecialised = res['specialisationOf']
    if itSpecialised == aConcept:
        return True
    if itSpecialised == _ConceptPID:  # ensure recursion terminates
        return False
    return isaSpecialisationOf( get(itSpecialised), # look a level further up
                                aConcept )

def getResultAsExpected( res,  # test whether the results, res, returned by get
          tup ):                # meet the expectations recorded in tup for a Concept
    # test every returned attribute as far as possible
    assert isaConcept(res), 'Not a Concept because \'instanceOf\' != Concept Concept PID'
    assert 'name' in res, 'Attribute name missing'
    assert res['name'] == tup[_name], 'name = \'' + res['name'] + \
           '\' it should be \'' + tup[_name] + '\''
    assert 'specialisationOf' in res, 'Attribute specialisationOf missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['specialisationOf'] == tup[_super], 'It specialises \'' + res['specialisationOf'] + \
           '\' whereas it should specialise \'' + tup[_super] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'instanceOf' in res, 'Attribute instanceOf missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['instanceOf'] == _ConceptPID, 'It is an instance of \'' + res['instanceOf'] + \
           '\' whereas it should an instance of Concept, i.e., \''  + _ConceptPID + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'prefix' in res, 'Attribute prefix missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['prefix'] == 'mpa', 'It has prefix \'' + res['prefix'] + \
           '\' whereas it should say it was made in \'' + 'mpa' + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'pid' in res, 'Attribute pid missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['pid'] == tup[_pid], 'PID \'' + res['pid'] + '\' not \'' + tup[_pid] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'state' in res, 'Attribute state missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['state'] == 'new', 'state \'' + res['state'] + '\' is not \'' + 'new' + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'timestamp' in res, 'No timestamp returned' + \
           ' for \'' + tup[_name] + '\''
    assert isinstance(res['timestamp'], dt.datetime), "timestamp not a datetime" + \
           ' for \'' + tup[_name] + '\''
    assert 'extra' in res, 'Attribute extra missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['extra'], mtDic, tup[_name]), 'Property \'extra\' is not an empty dictionary' + \
           ' for \'' + tup[_name] + '\''
    assert 'mutability' in res, 'Attribute mutability missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['mutability'] == 'mutable', 'Attribute \'mutable\' not set tp \'mutable\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'required' in res, 'Attribute required missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['required'], tup[_reqs], tup[_name] + '.required'), 'required is ' + str(res['required']) + \
                    ' but it should be ' + str(tup[_reqs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'recommended' in res, 'Attribute recommended missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['recommended'], tup[_recs], tup[_name] + '.recommended'), 'recommended is ' + str(res['recommended']) + \
                    ' but it should be ' + str(tup[_recs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'optional' in res, 'Attribute optional missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['optional'], tup[_opts], tup[_name] + '.optional'), 'optional is ' + str(res['optional']) + \
                    ' but it should be ' + str(tup[_opts]) + \
           ' for \'' + tup[_name] + '\''
    assert 'translation' in res, 'Attribute translation missing' + \
           ' for \'' + tup[_name] + '\''
    #if _extra_working:
    # assert eqDic(res['translation'], tup[_trans], tup[_name]), 'translation is ' + str(res['translation']) + \
    #                      ' but it should be ' + str(tup[_trans])
    assert 'description' in res, 'Attribute description missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['description'] == tup[_descr], 'description is \n' + str(res['description']) + \
                    '\nbut it should be\n' + str(tup[_descr]) + \
           ' for \'' + tup[_name] + '\''
    return True

def specialisation_of(putative_spcialisation_pid, target_pid)-> bool:
    suspect = dkb.get(putative_spcialisation_pid)
    return isaSpecialisationOf(suspect, target_pid)

_separator = ':'
def is_pid(p: str)-> bool:
    return 3 == pid.count(_separator)

def pid_of(name: str) -> str:
    assert name in _newConceptsDict, 'Concept name ' + name + ' not known'
    return _newConceptsDict[name][_pid]

def instance_of( inst: dict, Concept: str) -> bool:
    if not is_pid(Concept):
        Concept = pid_of(Concept)
        if inst['instanceOf'] == Concept:
            return True
        else:
            return specialisation_of(inst['instanceOf'], Concept)

_op = 0
_attr = 1
_val = 2
_conc = 1
def validate_list( res: list, query: list ):
    # check each returned value satisfies the query (no check that there aren't others that should be returned)
    for inst in res:
        if type(inst) == str:   # is it a PID?
            item = dkb.get(inst)
        else:
            item = inst
        assert type(item) == dict, 'Each item should now be a dictionary from attribute to value'    
        for cond in query:     # for each condition in the query
            if cond == 'AND':
                continue
            op = cond[_op]
            if op == '==':
                attr = cond[_attr]
                assert attr in item, 'Expect attribute ' + attr + ' in instance ' + str(item)
                val = cond[_val]
                assert item[attr] == val, 'Expected value of ' + attr + ' to be ' + \
                                        ' in instance ' + str(item)
            elif op == 'isa':
                conc = cond[_conc]
                assert 'instanceOf' in item, 'Expect attribute instanceOf in instance ' + str(item)
                assert instance_of(item, conc), 'Instance not an instance of ' + name_of(conc) + \
                                                 ' in instance ' + str(item)
            elif op == 'isa_exactly':
                conc = cond[_conc]
                assert 'instanceOf' in item, 'Expect attribute instanceOf in instance ' + str(item)
                assert item['instanceOf'] == pid_of(conc), "Not a direct instance of " + name_of(conc) + \
                                                 ' in instance ' + str(item)
            else:
                assert False, "Malformed query " + str(query) + ' found handling ' + str(cond)
    return True
                

def find_Concepts_by_name():
    for nom in _newConceptsDict:
        tup = _newConceptsDict[nom]
        nomSaved = tup[_name]      # the saved Concept name
        assert nom == nomSaved, 'inexplicable inconsitency in saved Concept data'
        if nom != nomSaved:		   # the test code itself is broken
            return
        query = ['AND', ('==', 'prefix', _concept_prefix), ('==', 'name', nom)]
        printv( '\nTrying query ' + str(query) + ' with nom = ' + nom + ' and prefix = ' + _concept_prefix )
        res = dkb.find(query)
        assert type(res) == list, 'Expected a list of PIDs for name ' + nom
        assert len(res) == 1, nom + ' did not retrieve exactly one Concept'
        assert res[0] == tup[_pid], 'Returned PID did not match remembered PID for ' + nom
        res = dkb.find(query, pid_only = False)
        assert len(res) == 1, nom + ' did not retrieve exactly one Concept for pid_only = False'
        res_spec = res[0]        # list of dictionaries like the ones returned by get expected
        assert getResultAsExpected(res_spec, tup), 'get result for \'' + nom + '\' not as expected'
        wanted = ['name', 'pid', 'prefix', 'specialisationOf', 'description']
        otlen = len(wanted)
        res = dkb.find(query, only_these = wanted)
        assert len(res) == 1, nom + ' did not retrieve exactly one Concept for only_these = [...]'
        res_spec = res[0]
        assert len(res_Spec) == otlen, nom + ' did not retrieve a dictionary of the right length'
        for prop in wanted:
            assert prop in res_spec, 'Property ' + prop + " not found in " + nom
        assert res_spec['name'] == nom. nom + " did not retrieve a correct name"
        assert res_spec['pid'] == tup[_pid], nom + "did not contain the right PID"
        assert res_spec['prefix'] == _concept_prefix, nom + "did not contain the right prefix " + _concept_prefix
        assert res_spec['specialisationOf'] == tup[_super], nom + ' should specialise ' + tup[_super]
        assert res_spec['description'] == tup[_descr], nom + ' desciption not ' + tup[_descr]

_instance_file_name = 'mpasInstances.pkl'
_instances_already_restored = False
_instances_prefix = ''
_newInstancesDict = None
def restore_pickled_instances():
    global _instances_already_restored, _instances_prefix, _newInstancesDict
    if _instances_already_restored:
        return
    _instances_already_restored = True
    pickle_path = _pickling_dir + _instance_file_name
    # already checked the pickling directory exists when restoring Concepts
    if not os.path.isfile(pickle_path):
        print( 'Pickled instances file ' + pickle_path + ' not found; Instances not recovered.' + 
                'Running\npytest ex3newInstance_test.py\nshould fix it.' )
        exit
    with open(pickle_path, 'rb') as input:
        _instances_prefix, _newInstancesDict = pickle.load(input)
        input.close()
    if _instances_prefix != _concept_prefix:
        dkb.enter(_instances_prefix, 'R')


def includes(res: dict, expected: dict, query: list) -> bool:
    assert len(res) >= len(expected), 'Not enough items in returned dictionary for ' + str(query)
    for prop in expected:
        value = expected[prop]
        assert prop in res, 'Attribute ' + prop + ' not found in the result for ' + str(query)
        assert res[prop] == value, 'Attribute ' + prop + ' did not have value ' + str(value) + \
                                   ' in the result for ' + str(query)
    return True

_inst_name = 0		# index for name
_inst_of = 1		# the Concept this is an instance of
_inst_attribs = 2	# the supplied properties
_inst_PID = 3       # the resulting PID

def test_find_Concepts():      # restore & test against saved Concepts
    global dkb
    set_controls()
    restore_pickled_Concepts()
    dkb = DKB.login('ex3KB',
            userName = 'mpa')     
    # work in the mpa Context
    dkb.enter(_concept_prefix, 'R')
    find_Concepts_by_name()
    
def test_find_Concept_groups():
    # test whether results that should yield a set of known size or known lower bound, actually do
    query = ['AND', ('==', 'prefix', _concept_prefix)]
    res = dkb.find(query, pid_only = False)
    assert type(res) == list, 'expected a list of dicts for prefix ' + _concept_prefix
    assert len(res) >= len(_newConceptsDict), 'expected at least an instance for each Concept in ' + _concept_prefix
    assert validate_list(res, query), 'At least one member of result doesn\'t satisfy ' + str(query)
    query = ['AND', ('==', 'prefix', _concept_prefix), ('isa', _ConceptPID)]
    res = dkb.find(query, pid_only = False)
    assert type(res) == list, 'expected a list of dicts for prefix ' + _concept_prefix
    assert len(res) == len(_newConceptsDict), 'expected an instance for each Concept in ' + _concept_prefix
    assert validate_list(res, query), 'At least one member of result doesn\'t satisfy ' + str(query)
    query = ['AND', ('==', 'prefix', _concept_prefix), ('isa_exactly', _ConceptPID)]
    res = dkb.find(query, pid_only = False)
    assert type(res) == list, 'expected a list of dicts for prefix ' + _concept_prefix
    assert len(res) == len(_newConceptsDict), 'expected an instance for each Concept in ' + _concept_prefix
    assert validate_list(res, query), 'At least one member of result doesn\'t satisfy ' + str(query)
    query = ['AND', ('==', 'prefix', _concept_prefix), ('==', 'specialisationOf', 'CA')]    # all specialisations of CA
    res = dkb.find(query, pid_only = False)
    assert type(res) == list, 'expected a list of dicts for specialisations of CA'
    assert len(res) == len(_newConceptsDict), 'expected an instance for each Concept in ' + _concept_prefix
    assert validate_list(res, query), 'At least one member of result doesn\'t satisfy ' + str(query)
    query = ['AND', ('==', 'prefix', _concept_prefix), ('==', 'specialisationOf', _newConceptsDict['CA'][_pid])]   # immeiate special
    res = dkb.find(query, pid_only = False)
    assert type(res) == list, 'expected a list of dicts for specialisations of CA'
    assert len(res) == len(_newConceptsDict), 'expected an instance for each Concept in ' + _concept_prefix
    assert validate_list(res, query), 'At least one member of result doesn\'t satisfy ' + str(query)

def test_find_instances_by_name():
    restore_pickled_instances()        # may reset prefix and enter the different Context
    for nom in _newInstancesDict:
        tup = _newInstancesDict[nom]
        query = ['AND', ('==', 'prefix', _instances_prefix), ('==', 'name', nom)]
        res = dkb.find(query)
        assert type(res) == list, 'Expected a list of PIDs for name ' + nom
        assert len(res) == 1, nom + ' did not retrieve exactly one instance'
        assert res[0] == tup[_inst_PID], 'Returned PID did not match remembered PID for ' + nom
        assert validate_list(res, query), 'Retrying query on returned list failed for ' + \
                                          nom + ' for query ' + str(query)
        query = ['AND', ('==', 'prefix', _instances_prefix), ('==', 'name', nom), ('isa_exactly', tup[_inst_of])]
        res = dkb.find(query)
        assert type(res) == list, 'Expected a list of PIDs for name ' + nom
        assert len(res) == 1, nom + ' did not retrieve exactly one instance'
        assert res[0] == tup[_inst_PID], 'Returned PID did not match remembered PID for ' + nom
        assert validate_list(res, query), 'Retrying query on returned list failed for ' + \
                                          nom + ' for query ' + str(query)
        res = dkb.find(query, pid_only = False)
        assert type(res) == list, 'Expected a list of PIDs for name ' + nom
        assert len(res) == 1, nom + ' did not retrieve exactly one instance'
        assert validate_list(res, query), 'Retrying query on returned list failed for ' + \
                                          nom + ' for query ' + str(query)
        assert includes(res, tup[_inst_attribs], query), 'Response did not match expected for ' + \
                                                   nom + ' for query ' + str(query)

def test_find_instances_by_content():
    pass

def test_find_instance_groups():
    pass
    
def test_find_exceptions():
   # with pytest.raises(TypeError):
   #     err1 = dkb.find( 'CAZZZZZZ' )	  # query not a list
   # with pytest.raises(TypeError):
   #     err2 = dkb.find( [True] )	      # list element not a triple or pair
   # with pytest.raises(TypeError):
   #     err3 = dkb.find( [('===', 'name', 'nemo')] ) # unrecognised conditional operator
    with pytest.raises(Exceptions.AttributeError):
        err4 = dkb.find( [('==', 'unknownattributename', 'nemo')] ) # unrecognised attribute
    dkb.close()
    with pytest.raises(Exceptions.DKBclosedError):
        err5 = dkb.find([('==', 'name', 'Widget')])	  