# Ex3 
# Test function to initialise a DKB instance

import DKBlib as DKB
from DKBlib import Exceptions
import pytest

def test_firstOpen():
    # start a new DKB setting it to its initial empty state
    dkb = DKB.newDKB('ex3KB') # Assume this is the first time
    assert dkb == None	# check nothing returned

def test_secondOpenWithReset():
    # check can be opened with reset = True
    DKB.newDKB('ex3KB',      # Second time but allow reset
            reset = True)

def test_exceptionCheck():   
    # check for anticipated exceptions
    with pytest.raises(Exceptions.DKBnotResetError):
        DKB.newDKB('ex3KB' )	# try to make again without reset = True


