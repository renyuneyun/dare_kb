# Ex3 pytest that determines whether pytest ex3newConcept_test.py worked 
# Test function get for the simple examples in one Context (mpa) made by ex3newConcepttest
 
import DKBlib as DKB
import pytest 
from DKBlib import Exceptions
import datetime as dt
import sys 
import pickle
import os

_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept
_verbose = False
_extra_working = True

mtDic = {}               # many occasions an empty dictionary is needed
    
_name = 0		# index for name
_super = 1		# the Concept this specialises
_reqs = 2		# the required properties
_recs = 3		# the recommended properties
_opts = 4       # the optional properties
_trans = 5      # the translation of property names
_pid = 6		# the identifier returned by newConcept
_descr = 7		# the description of the Concept

# The attributes every instance in the DKB has automatically
_builtInAttributes = ['pid', 'prefix', 'instanceOf', 'name', 'timestamp', 'state']

dkb = None						# global shared DKB instance variable set at start

    # save a record of Concepts created in a form that is easily pickled and restored
_newConceptsDict = dict()		# a dictionary mapping names to tuples
_pickling_dir = '../../pickles/'     # place where pickle files are stored
_file_name = 'mpasConcepts.pkl'
_the_context = 'nemo'           # the one we use, matches the last one used for newConcept
_alreadyRestored = False
def restoreRememberedConcepts(): # recover from the file made by saveRememberedConcepts
    global _newConceptsDict, _alreadyRestored, _the_context
    if _alreadyRestored:
        return
    _alreadyRestored = True
    if not os.path.isdir(_pickling_dir):
        print( 'Directory for pickling ' + _pickling_dir + ' not found; Concepts not recovered')
        exit
    pickle_path = _pickling_dir + _file_name
    if not os.path.isfile(pickle_path):
        print( 'Pickled Concept file ' + pickle_path + ' not found; Concepts not recovered.' +
         'Run \npytest ex3newConcept_test.py\nto create the Concepts' )
        exit
    with open(_pickling_dir + 'mpasConcepts.pkl', 'rb') as input:
        _the_context, _newConceptsDict = pickle.load(input)

def printDict(dic,
            indent = ''):			# print the supplied dictionary 
    for k in dic:
        print(indent + 
              k + ': ' + str(dic[k]))
              
def printGetResult(res):   #print what is returned by get, irrespective of what it is
    if not _verbose:
        return
    if not isinstance(res, dict):
        for k in res:
            print( k + ': ' +
                   str(res[k]))
               
def eqDic(dic1: dict, dic2:dict,     # true iff the two supplied dictionaries are equal
          name:str) -> bool:
    assert len(dic1) == len(dic2), 'The two dictionaries, \'' + str(dic1) + '\' and \'' + \
                      str(dic2) + '\' have different lengths for ' + name 
    if len(dic1) != len(dic2):
        return False
    for key in dic1:
        assert key in dic2, '\'' + key + '\' in ' + str(dic1) + \
                        ' but not in ' + str(dic2)
        if not key in dic2:
            return False
        assert dic1[key] == dic2[key], 'The values for \'' + key + '\' differ' 
        if dic1[key] != dic2[key]:
            return False
    return True
    
def isaConcept(res):            # given a dictionary returned by a get test if it is
                                # a specialisation directly or indirectly of Concept
    return res['instanceOf'] == _ConceptPID
    
def isaSpecialisationOf(res,    # test whether the dictionary represents a 
                        aConcept): # directly or indirectly of the supplied Concept
    assert res.has_key('specialisationOf'), 'No specialisationOf attribute present => not a well-formed Concept'
    if not res.has_key('specialisationOf'):
        return False
    itSpecialised = res['specialisationOf']
    if itSpecialised == aConcept:
        return True
    if itSpecialised == _ConceptPID:  # ensure recursion terminates
        return False
    return isaSpecialisationOf( get(itSpecialised), # look a level further up
                                aConcept )

def printConcept(res):          # given a result returned by get, print it as a Concept
    assert isaConcept(res), 'Asking printConcept to print something that isn\'t a Concept'
    if not isaConcept(res):
        return
    print('\nComcept ' + res['name'] + ' created at time ' + str(res['timestamp']) +
        ' in Context ' + res['prefix'] + ' given PID ' + res['pid'] +
        ' with state = ' + res['state']) # start with standard attributes on a line
    indent = ' ' * 4
    specialises = res['specialisationOf']
    if specialises != _ConceptPID:
        print(indent + 'Specialises ' + specialises)
    extra = res['extra']
    if len(extra) > 0:	# extra is normally empty
        print(indent + 'extra = ' )
        printDict(extra, indent = ' ' * 8)
    if res['mutability'] == 'mutable':
        print(indent + 'It is mutable')
    else:
        print(indent + "mutability = " + res['mutability'])
    required = res['required']
    if len(required) > 0:       # are there required attributes
        printDict(required, indent = indent)
    recommended = res['recommended']
    if len(recommended) > 0:       # are there recommended attributes
        print(indent + '---')      # the indicator for recommended attributes follow
        printDict(recommended, indent = indent)
    optional = res['optional']
    if len(optional) > 0:          # are there optional attributes
        print(indent + '...')      # the indicator for optional attributes follow
        printDict(optional, indent = indent)
    translation = res['translation']
    if len(translation) > 0:       # are there translations of attribute ids?
        print(indent + 'translation =')      # the indicator for recommended attributes follow
        printDict(translation, indent = ' ' * 8)
    description = res['description']
    if description == None:
        return
    if description != '':
        print(indent + 'description =')
        print(description)         # no indent to finish off  
    

def getResultAsExpected( res,  # test whether the results, res, returned by get
          tup ):                # meet the expectations recorded in tup for a Concept
    # test every returned attribute as far as possible
    assert isaConcept(res), 'Not a Concept because \'instanceOf\' != Concept Concept PID'
    assert 'name' in res, 'Attribute name missing'
    assert res['name'] == tup[_name], 'name = \'' + res['name'] + \
           '\' it should be \'' + tup[_name] + '\''
    assert 'specialisationOf' in res, 'Attribute specialisationOf missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['specialisationOf'] == tup[_super], 'It specialises \'' + res['specialisationOf'] + \
           '\' whereas it should specialise \'' + tup[_super] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'instanceOf' in res, 'Attribute instanceOf missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['instanceOf'] == _ConceptPID, 'It is an instance of \'' + res['instanceOf'] + \
           '\' whereas it should an instance of Concept, i.e., \''  + _ConceptPID + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'prefix' in res, 'Attribute prefix missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['prefix'] == _the_context, 'It has prefix \'' + res['prefix'] + \
           '\' whereas it should say it was made in \'' + _the_context + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'pid' in res, 'Attribute pid missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['pid'] == tup[_pid], 'PID \'' + res['pid'] + '\' not \'' + tup[_pid] + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'state' in res, 'Attribute state missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['state'] == 'new', 'state \'' + res['state'] + '\' is not \'' + 'new' + '\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'timestamp' in res, 'No timestamp returned' + \
           ' for \'' + tup[_name] + '\''
    assert isinstance(res['timestamp'], dt.datetime), "timestamp not a datetime" + \
           ' for \'' + tup[_name] + '\''
    assert 'extra' in res, 'Attribute extra missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['extra'], mtDic, tup[_name]), 'Property \'extra\' is not an empty dictionary' + \
           ' for \'' + tup[_name] + '\''
    assert 'mutability' in res, 'Attribute mutability missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['mutability'] == 'mutable', 'Attribute \'mutable\' not set tp \'mutable\'' + \
           ' for \'' + tup[_name] + '\''
    assert 'required' in res, 'Attribute required missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['required'], tup[_reqs], tup[_name] + '.required'), 'required is ' + str(res['required']) + \
                    ' but it should be ' + str(tup[_reqs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'recommended' in res, 'Attribute recommended missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['recommended'], tup[_recs], tup[_name] + '.recommended'), 'recommended is ' + str(res['recommended']) + \
                    ' but it should be ' + str(tup[_recs]) + \
           ' for \'' + tup[_name] + '\''
    assert 'optional' in res, 'Attribute optional missing' + \
           ' for \'' + tup[_name] + '\''
    assert eqDic(res['optional'], tup[_opts], tup[_name] + '.optional'), 'optional is ' + str(res['optional']) + \
                    ' but it should be ' + str(tup[_opts]) + \
           ' for \'' + tup[_name] + '\''
    assert 'translation' in res, 'Attribute translation missing' + \
           ' for \'' + tup[_name] + '\''
   # assert eqDic(res['translation'], tup[_trans], tup[_name]), 'translation is ' + str(res['translation']) + \
   #                       ' but it should be ' + str(tup[_trans])
    assert 'description' in res, 'Attribute description missing' + \
           ' for \'' + tup[_name] + '\''
    assert res['description'] == tup[_descr], 'description is \n' + str(res['description']) + \
                    '\nbut it should be\n' + str(tup[_descr]) + \
           ' for \'' + tup[_name] + '\''
    return True
    
    
def test_getAppliedToStoredConcepts():
    global dkb, _newConceptDict, _verbose
#    if metafunc.config.getoption("all"):
#        _verbose = True 			# switch on print to std out
    
    # login to a DKB in its current state state and start to use it
    dkb = DKB.login('ex3KB',
            userName = 'mpa') 
    # work in the Context used to perform newConcept last time
    restoreRememberedConcepts()    # recover a copy made during newConcept test
    dkb.enter(_the_context, 'R')
    # check whether each one is there and is reachable by all three identifiers
    for nom in _newConceptsDict:    # get each saved tuple
        tup = _newConceptsDict[nom]
        nomSaved = tup[_name]      # the saved Concept name
        assert nom == nomSaved, 'inexplicable inconsitency in saved Concept data'
        if nom != nomSaved:		   # the test code itself is broken
            return
        byNom = dkb.getConcept(nom)       # by simple name; it should be local in Context mpa
        byPrefNom = dkb.getConcept(_the_context + ':' + nom) # by explicit local prefix:name
        byPID = dkb.getConcept(tup[_pid]) # by explicit full PID
        printGetResult(byNom)      # if verbose see what was returned in one case!
        assert getResultAsExpected(byNom, tup), 'get result for \'' + nom + '\' not as expected' 
        assert getResultAsExpected(byPrefNom, tup), 'get result for \'' + _the_context + ':' + nom + \
                         '\' not as expected' 
        assert getResultAsExpected(byPID, tup), 'get result for \'' + tup[_pid] + \
                         '\' not as expected' 


def test_getExceptions():       # These exception names need correcting
    with pytest.raises(Exceptions.NotAConceptError):
        err1 = dkb.getConcept( 'CAZZZZZZ')	  # unknown name
    with pytest.raises(Exceptions.NotAConceptError):
        err1 = dkb.getConcept( _the_context + ':CAZZZZZZ')	  # unknown name
    with pytest.raises(Exceptions.ContextNotFoundError):
        err1 = dkb.getConcept( 'nemo:CA')	  # unknown prefix
    with pytest.raises(Exceptions.NotAConceptError):
        CA2 = dkb.getConcept( 'ex3:' + _the_context + ':1299:CA')	  # PID to a non-existent entry
    dkb.close()
    with pytest.raises(Exceptions.DKBclosedError):
        CA2 = dkb.getConcept( 'CA')	  


