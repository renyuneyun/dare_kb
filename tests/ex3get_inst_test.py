# Ex3 pytest that determines whether pytest ex3newInstance_test.py worked 
# Test function get for the simple examples in one Context (mpa) made by ex3newConcepttest
 
import DKBlib as DKB
import pytest 
from DKBlib import Exceptions
import datetime as dt
import sys 
import pickle
import os

_picklingDir = '../../pickles/'     # place where pickle files are stored
_ConceptPID = 'ex3KB:kb:1:Concept' # expected PID for the predefined Concept Concept
_verbose = False
mtDic = {}               # many occasions an empty dictionary is needed

# The attributes every instance in the DKB has automatically
_builtInAttributes = ['pid', 'prefix', 'instanceOf', 'name', 'timestamp', 'state']

dkb = None						# global shared DKB instance variable set at start

_newInstancesDict = dict()		# a dictionary mapping instance names to tuples

_alreadyRestored = False
_instances_prefix = ''
def restoreRememberedInstances():    # recover a copy of instances made during pytest ex3newInstance_test.py
    global _newInstancesDict, _alreadyRestored, _instances_prefix
    if _alreadyRestored:
        return
    _alreadyRestored = True
    if not os.path.isdir(_picklingDir):
        print( 'Directory for pickling ' + _picklingDir + ' not found; Concepts not recovered')
        exit
    pickle_path = _picklingDir + 'mpasInstances.pkl'
    if not os.path.isfile(pickle_path):
        print( 'Pickled Concept file ' + pickle_path + ' not found; Concepts not recovered\n' +
               'Perform \npytest -s ex3newInstance_test.py\nto make them.')
        exit
    with open(pickle_path, 'rb') as input:
        _instances_prefix, _newInstancesDict = pickle.load(input)
        input.close()
        print('_instances_prefix = ' + _instances_prefix)

# offsets to unpack pickled tuples
_name = 0		# index for name
_instOf = 1		# the Concept this is an instance of
_attribs = 2	# the supplied properties
_instPID = 3    # the resulting PID

def asExpected( res: dict, tup: tuple ) -> bool:
    # test every returned attribute as far as possible
    setProperties = tup[_attribs]
    assert 'name' in res, 'Attribute name missing'
    assert res['name'] == tup[_name], 'name = \'' + res['name'] + \
           '\' it should be \'' + tup[_name] + '\''
    assert 'instanceOf' in res, 'Attribute instanceOf missing'
    assert res['instanceOf'] == tup[_instOf], 'It is an instance of \'' + res['instanceOf'] + \
           '\' whereas it should an instance of '  + tup[_instOf]
    assert 'prefix' in res, 'Attribute prefix missing'
    assert res['prefix'] == _instances_prefix, 'It has prefix \'' + res['prefix'] + \
           '\' whereas it should say it was made in \'' + _instances_prefix + '\''
    assert 'pid' in res, 'Attribute pid missing'
    assert res['pid'] == tup[_instPID], 'PID \'' + res['pid'] + '\' not \'' + tup[_instPID] + '\''
    assert 'state' in res, 'Attribute state missing'
    if not 'state' in setProperties:    # then the default
        assert res['state'] == 'new', 'state \'' + res['state'] + '\' is not \'' + 'new' + '\''
    assert 'timestamp' in res, 'No timestamp returned'
    assert isinstance(res['timestamp'], dt.datetime), "timestamp not a datetime"
    # assert 'extra' in res, 'Attribute extra missing'  # may contain unexpected properties
    # extraDic = res['extra']
    assert 'mutability' in res, 'Attribute mutability missing'
    if not 'mutability' in setProperties:  # then the default
        assert res['mutability'] == 'mutable', 'Attribute \'mutable\' not set tp \'mutable\''
                 # check that each of these specified properties was recorded
    for prop in setProperties:
        if prop in res:
            assert res[prop] == setProperties[prop]   # an expected property
     #   else:
     #       assert prop in extraDic, "Extra attribute not in extra"
     #       assert extraDic[prop] == setProperties[prop]   # an unexpected property
    return True

def test_getAppliedToStoredInstances():
    global dkb    
    restoreRememberedInstances()    # recover a copy made during pytest ex3newInstance_test.py in recovered Context
    # login to a DKB in its current state and start to use it
    dkb = DKB.login('ex3KB',
            userName = 'mpa') 
    # work in the mpa Context
    dkb.enter(_instances_prefix, 'R')
    for nom in _newInstancesDict:
        tup = _newInstancesDict[nom] # get the record of one instance saved during pytest ex3newInstance_test.py
        nameSaved = tup[_name]
        assert nom == nameSaved, "pickled data not consistent"
        byName = dkb.get(nom)                # try all three identification models
        assert asExpected(byName, tup), "get(name) not as expected"
        byPrefixName = dkb.get(_instances_prefix + ':' + nom)
        assert asExpected(byPrefixName, tup), "get(prefix:name not as expeced)"
        byPID = dkb.get(tup[_instPID])
        assert asExpected(byPID, tup), "get(PID) not as expected"