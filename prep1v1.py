# prep1 the first preparation step 
# the preparations are to help with/ check and stimulate the DKB design
# This one generates a new DKB instance DKB1 as it appears in the examples in
# the DKB design. It resets the instance, to get to a standard state 
# It then sets up a few contexts" dare and ex at least.
# dare will be used for the preloaded entries
# ex will be used in the running examples
 
import DKB_functions as DKBm
import datetime as dt
import sys 

_builtInAttributes = ['identity', 'instanceOf', 'name']
_logDir = '/Users/malcolm/tmp_files/'		# place to put files with error logs

# error report will eventually report to a file if there are errors
_errorCount = 0          #number errors to help issue tracking 
_errorFile  = None       #create output error file if errors detected
_verbose = True          #iff true also report on standard out

def errorReport(error):
    global _verbose, _errorFile, _errorCount, _logDir
    _errorCount += 1
    if _verbose:
        print (_errorCount, error, '\n')
    if _errorFile == None:    # first error, create the file 
        now = dt.datetime.utcnow() #when it happened
        errFileName = _logDir + 'ErrorLog' + '+' + now.ctime()
        _errorFile = open(errFileName, mode=  'w')
    _errorFile.write(str(_errorCount)) 
    _errorFile.write(' ' + error + '\n')


def vprint(toPrint): #print only if _verbose is true
    global _verbose
    if _verbose:
        print(toPrint)


def mainInner():
    global _errorCount
    # start a new DKB setting it to its initial empty state
    dkb = DKBm.initDKB('DKB1',
                        reset = True) # restore to initial state
    
    dare = dkb.newContext(
      'Context for built in and universally shared entries',	# the title
      'dare'							# the prefix
    )
    dkb.setContext('dare')
    
    # retain some testing
    cc = dkb.getContext()					# repeating the function pattern
    if cc == None:
        errorReport('Context not retrieved by getContext')
    else: 
        if cc.title != 'Context for built in and universally shared entries':
            errorReport('title not retrieved by getContext')
        if cc.prefix != 'dare':
            errorReport('prefix not retrieved by getContext')

    if dkb.currentContext == None:
        errorReport('currentContext not set')
    
    dkb.close()

def main():
    global _errorCount
    try: 			# catch unexpected exceptions here
        mainInner()
    finally:
        ex = sys.exc_info() # get an uncaught exception if any
        if ex[1] != None:
            # here an uncaught exception
            errorReport('Uncaught exception ' + str(ex[0]) + ':' + str(ex[1]))
        if _errorCount > 0:
            _errorFile.close()
    vprint(str(_errorCount)+' errors detected')
    
if __name__=='__main__':     
    main()
